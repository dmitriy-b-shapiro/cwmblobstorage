using System;
using System.Linq;
using System.Security.Cryptography;
using Cwm.Common.Encryption;
using Cwm.Common.Exceptions;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Cwm.Data.Azure.Tenant;
using Humanizer;

namespace Cwm.Data.Azure.KeyVault
{
    internal class KeyFetcher : IKeyFetcher
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(KeyFetcher));
        private readonly ITenantRepository _tenantRepository;
        private readonly IKeyVaultClient _keyVaultClient;
        private readonly IMemoryCache _cache;

        public KeyFetcher(ITenantRepository tenantRepository, IKeyVaultClient keyVaultClient, IMemoryCache cache)
        {
            _tenantRepository = tenantRepository;
            _keyVaultClient = keyVaultClient;
            _cache = cache;
        }

        public Aes GetKey(IUserSession userSession, Common.Models.Tenant tenant)
        {
            if (tenant == null)
                throw new BadRequestException("Tenant not found for user {0}".FormatWith(userSession.HashKey));

            var cacheKey = "key|{0}".FormatWith(tenant.Environment);
            return _cache.GetOrAdd(userSession, cacheKey, () =>
            {
                var secret = _keyVaultClient.GetSecretSync<string>(userSession, tenant.DecodedIdentifier);
                if (secret == null)
                    throw new BadRequestException("Encryption key not found for user {0}".FormatWith(userSession.HashKey));

                return CryptoUtility.CreateSymmetricKey(secret);
            }, 1200);
        }

        public Aes GetKey(IUserSession userSession)
        {
            var tenant = TenantsCache.Read(userSession.TenantId, () => _tenantRepository.GetTenants());
            if (tenant == null)
            {
                Log.Error($"Unable to find tenant for user {userSession.TenantId}|{userSession.UserId}");
            }
            return GetKey(userSession, tenant);
        }
    }
}