using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cwm.Common;
using Cwm.Common.Interfaces;

namespace Cwm.Data.Azure.KeyVault
{
    public class MockedKeyVaultClient : IKeyVaultClient
    {
        private Secrets _secrets;
        
        public Task<T> GetSecret<T>(string key) where T : class
        {
            if (_secrets == null)
                _secrets = Utilities.DeserializeFromXmlString<Secrets>(Utilities.Extract("Cwm.Data.Azure.KeyVault.MockedKeyVault.xml"));

            var secret = _secrets.secret.FirstOrDefault(s => s.id.Equals(key, StringComparison.OrdinalIgnoreCase));
            return Task.FromResult(secret != null ? secret.Value as T : default(T));
        }

        public Task<List<string>> GetSecretNames(IUserSession userSession)
        {
            throw new NotImplementedException();
        }

        public Task DeleteSecret(IUserSession userSession, string key)
        {
            throw new NotImplementedException();
        }

        public Task<string> CreateSecret(IUserSession userSession, string key, object value)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetSecret<T>(IUserSession userSession, string key) where T : class
        {
            if (_secrets == null)
                _secrets = Utilities.DeserializeFromXmlString<Secrets>(Utilities.Extract("Cwm.Data.Azure.KeyVault.MockedKeyVault.xml"));

            var secret = _secrets.secret.FirstOrDefault(s => s.id.Equals(key, StringComparison.OrdinalIgnoreCase));
            return Task.FromResult(secret != null ? secret.Value as T : default(T));
        }

        public T GetSecretSync<T>(IUserSession userSession, string key) where T : class
        {
            if (_secrets == null)
                _secrets = Utilities.DeserializeFromXmlString<Secrets>(Utilities.Extract("Cwm.Data.Azure.KeyVault.MockedKeyVault.xml"));

            var secret = _secrets.secret.FirstOrDefault(s => s.id.Equals(key, StringComparison.OrdinalIgnoreCase));
            return secret != null ? secret.Value as T : default(T);
        }
    }
}