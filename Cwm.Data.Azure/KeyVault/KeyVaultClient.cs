using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Cwm.Common.Exceptions;
using Cwm.Common.Extensions;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Humanizer;
using Microsoft.Azure.KeyVault;
using Newtonsoft.Json;

namespace Cwm.Data.Azure.KeyVault
{
    public class KeyVaultClient : IKeyVaultClient
    {
        private readonly ITokenService _tokenService;
        private readonly ICwmConfig _config;
        private readonly string _vaultUrl;
        private static readonly ILog Log = LogManager.GetLogger(typeof(KeyVaultClient));

        public KeyVaultClient(ITokenService tokenService, ICwmConfig config)
        {
            _tokenService = tokenService;
            _config = config;

            _vaultUrl = "https://{0}.vault.azure.net/".FormatWith(_config.KeyVaultName);
        }

        private Microsoft.Azure.KeyVault.KeyVaultClient GetKeyVaultClient(IUserSession userSession)
        {
            Log.Info("Getting KeyVault client...");
            var token = _tokenService.GetApplicationLevelToken(userSession, "https://vault.azure.net", _config.ApplicationTenant);
            var kv = new Microsoft.Azure.KeyVault.KeyVaultClient((authority, resource, scope) => Task.FromResult(token.Token));
            Log.Info("Got KeyVault client.");

            return kv;
        }

        public async Task<List<string>> GetSecretNames(IUserSession userSession)
        {
            var keyVaultClient = GetKeyVaultClient(userSession);
            var secrets = await keyVaultClient.GetSecretsAsync(_vaultUrl);
            return secrets.Value.Select(item => item.Identifier.Name).ToList();
        }

        public async Task DeleteSecret(IUserSession userSession, string key)
        {
            var keyVaultClient = GetKeyVaultClient(userSession);
            try
            {
                await keyVaultClient.DeleteSecretAsync(_vaultUrl, key);
            }
            catch (KeyVaultClientException ex) { throw HandleError(ex); }
        }

        public async Task<string> CreateSecret(IUserSession userSession, string key, object value)
        {
            var keyVaultClient = GetKeyVaultClient(userSession);
            var secret = await keyVaultClient.SetSecretAsync(_vaultUrl, key, ObjectToSecretString(value));
            return secret.Value;
        }

        public async Task<T> GetSecret<T>(IUserSession userSession, string key) where T : class
        {
            var keyVaultClient = GetKeyVaultClient(userSession);
            try
            {
                var secret = await keyVaultClient.GetSecretAsync(_vaultUrl, key);
                return SecretStringToObject<T>(secret.Value);
            }
            catch (KeyVaultClientException ex) { throw HandleError(ex); }
        }

        public T GetSecretSync<T>(IUserSession userSession, string key) where T : class
        {
            var keyVaultClient = GetKeyVaultClient(userSession);
            try
            {
                var secret =  keyVaultClient.GetSecretAsync(_vaultUrl, key).GetAwaiter().GetResult();
                return SecretStringToObject<T>(secret.Value);
            }
            catch (KeyVaultClientException ex) { throw HandleError(ex); }
        }

        private static Exception HandleError(KeyVaultClientException ex)
        {
            if (ex.Status == HttpStatusCode.NotFound)
            {
                Log.Warn(ex.Message, ex);
                return new NotFoundException(ex.Message);
            }
            Log.Error(ex.Message, ex);
            return ex;
        }

        private static string ObjectToSecretString(object obj)
        {
            if (obj is string) return obj as string; // Encoding.Unicode.GetBytes(obj as string).ToBase64();
            return Encoding.Unicode.GetBytes(JsonConvert.SerializeObject(obj)).ToBase64();
        }

        private static T SecretStringToObject<T>(string s) where T : class
        {
            if (typeof(T) == typeof(string)) return s as T;
            return JsonConvert.DeserializeObject<T>(Encoding.Unicode.GetString(Convert.FromBase64String(s)));
        }
    }
}