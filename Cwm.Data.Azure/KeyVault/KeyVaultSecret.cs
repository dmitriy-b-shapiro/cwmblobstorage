namespace Cwm.Data.Azure.KeyVault
{
    internal class KeyVaultSecret
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Secrets
    {

        private SecretsSecret[] secretField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("secret")]
        public SecretsSecret[] secret
        {
            get
            {
                return this.secretField;
            }
            set
            {
                this.secretField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SecretsSecret
    {

        private string idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

}