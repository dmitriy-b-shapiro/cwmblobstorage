﻿using System.Reflection.Emit;
using Cwm.Common.Models;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cwm.Data.Azure.Tenant
{   

    public class TenantEntity : TableEntity
    {
        public TenantEntity()
        {
            Version = 3;
            RowKey = string.Empty;
            AuthType = TenantAuthenticationType.Default.ToString();
        }

        public void UpdatePropertiesFrom(TenantEntity tenant)
        {
            PartitionKey = tenant.PartitionKey;
            RowKey = tenant.RowKey;
            Timestamp = tenant.Timestamp;
            Identifier = tenant.Identifier;
            Name = tenant.Name;
            Version = 3;
            AuthType = tenant.AuthType;
            ResourceName = tenant.ResourceName;
            Url = tenant.Url;
            ClientId = tenant.ClientId;
            ClientKey = tenant.ClientKey;
            FlowSignup = tenant.FlowSignup;
            FlowAccount = tenant.FlowAccount;
            FlowPassword = tenant.FlowPassword;
            AuthScope = tenant.AuthScope;
        }

        public string Identifier { get; set; }
        public string Name { get; set; }
        public int Version { get; set; }

        #region Additional Authentication
        public string AuthType { get; set; }

        public string ResourceName { get; set; }
        public string Url { get; set; }
        public string ClientId { get; set; }
        public string ClientKey { get; set; }
        public string FlowSignup { get; set; }
        public string FlowPassword { get; set; }
        public string FlowAccount { get; set; }

        public string AuthScope { get; set; }

        #endregion

        public static TenantEntity FromTenant(Common.Models.Tenant tenant)
        {
            return new TenantEntity
            {
                PartitionKey = tenant.Environment,
                RowKey = tenant.TenantId,
                Identifier = tenant.Identifier,
                Name = tenant.Name,
                Version = 3,
                AuthType = tenant.AuthType.ToString(),
                ResourceName = tenant.ResourceName,
                Url = tenant.Url,
                ClientId = tenant.ClientId,
                ClientKey = tenant.ClientKey,
                FlowSignup = tenant.Flows.Signup,
                FlowAccount = tenant.Flows.Account,
                FlowPassword = tenant.Flows.Password,     
                AuthScope = tenant.AuthScope,
            };
        }

        public Common.Models.Tenant ToTenant()
        {
            return new Common.Models.Tenant
            {
                Name = Name,
                TenantId = RowKey,
                Environment = PartitionKey,
                Identifier = Identifier ?? string.Empty,
                AuthType = AuthTypeFromString(AuthType),
                ResourceName = ResourceName,
                Url = Url,
                ClientId = ClientId,
                ClientKey = ClientKey ?? string.Empty,
                AuthScope = AuthScope,
                Flows =
                {
                    Signup = FlowSignup,
                    Account = FlowAccount,
                    Password = FlowPassword,
                },
            };
        }

        private TenantAuthenticationType AuthTypeFromString(string type)
        {
            if (string.IsNullOrWhiteSpace(type)) return TenantAuthenticationType.Default;
            if (type.Equals("B2C")) return TenantAuthenticationType.B2C;
            //add others here
            return TenantAuthenticationType.Default;
        }
    }
}

