﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using Cwm.Common.Extensions;
using Cwm.Common.Models;

namespace Cwm.Data.Azure.Tenant
{
    public static class TenantsCache
    {
        /// <summary>
        /// This is a cache object used to retrieve the tenants.
        /// The tenants are kept in Azure table storage and accessed under the covers via a REST API. We don't want to
        /// Note: static ctor guaranteed to only be called by 1 thread
        /// </summary>
        private static readonly SimpleTenantCache Tenants = new SimpleTenantCache(Convert.ToInt32(ConfigurationManager.AppSettings["cwm.TenantCacheTimeoutInSeconds"]));


        public static List<Common.Models.Tenant> GetTenants(Func<List<Common.Models.Tenant>> fetchValue)
        {
            return Tenants.Get(fetchValue);
        }

        public static Common.Models.Tenant Read(string tenantId, Func<List<Common.Models.Tenant>> fetchValue)
        {
            var tenant = Tenants.Read(tenantId, fetchValue);
            return tenant;
        }
        

        public static Common.Models.Tenant TenantByUri(string uri, Func<List<Common.Models.Tenant>> fetchValue)
        {
            return Tenants.TenantByUri(uri, fetchValue);
        }


        public static string B2CDomain(string tenantId, Func<List<Common.Models.Tenant>> fetchValue)
        {
            var currentTenant = Tenants.Read(tenantId, fetchValue);
            var b2CTenant = Tenants.ReadB2CByName(currentTenant.Name, fetchValue);

            var b2CUrl = b2CTenant?.Url;
            return b2CUrl;
        }

        public static string TenantIdFromIssuer(string issuer)
        {
            var issuerTenantId = issuer.Length > 60 ? issuer.Substring(24, 36) : issuer;
            return issuerTenantId;
        }

        public static string TenantIdFromB2CIssuer(string b2cIssuer)
        {
            int index = b2cIssuer.GetNthIndex('/', 3);
            var tenantId = b2cIssuer.Substring(index + 1, 36);
            return tenantId;
        }

        private static int GetNthIndex(this string s, char t, int n)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == t)
                {
                    count++;
                    if (count == n)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
    }

    /// <summary>
    /// Provides a simple in-memory local cache. To use define it as a static field in an appropriate class.
    /// </summary>    
    public class SimpleTenantCache
    {
        private readonly int _expirationSeconds;
        private DateTime _cacheDirtyTime;
        private List<Common.Models.Tenant> _tenantList;
        private readonly Dictionary<string, Common.Models.Tenant> _tenantLookup = new Dictionary<string, Common.Models.Tenant>();
        private readonly Dictionary<string, Common.Models.Tenant> _uriTenantLookup = new Dictionary<string, Common.Models.Tenant>();
        private readonly Dictionary<string, Common.Models.Tenant> _b2CTenantLookup = new Dictionary<string, Common.Models.Tenant>();
        private readonly object _syncLock = new object();

        public SimpleTenantCache(int expirationSeconds = 60)
        {
            _expirationSeconds = expirationSeconds;
            ResetDirtyCacheTime();
        }

        private void ResetDirtyCacheTime()
        {
            _cacheDirtyTime = DateTime.UtcNow.AddSeconds(_expirationSeconds);
        }


        public List<Common.Models.Tenant> Get(Func<List<Common.Models.Tenant>> fetchValue)
        {
            Load(fetchValue);
            return _tenantList;
        }

        public Common.Models.Tenant Read(string tenantId, Func<List<Common.Models.Tenant>> fetchValue)
        {
            Load(fetchValue);
            return _tenantLookup.TryGetValue(tenantId, out var tenant) ? tenant : null;

        }
        
        public Common.Models.Tenant TenantByUri(string uri, Func<List<Common.Models.Tenant>> fetchValue)
        {
            Load(fetchValue);
            return _uriTenantLookup.TryGetValue(uri.ToLowerInvariant(), out var tenant) ? tenant : null;

        }

        public Common.Models.Tenant ReadB2CByName(string tenantName, Func<List<Common.Models.Tenant>> fetchValue)
        {
            Load(fetchValue);
            return _b2CTenantLookup.TryGetValue(tenantName, out var tenant) ? tenant : null;
        }


        private void Load(Func<List<Common.Models.Tenant>> fetchValue)
        {
            if (_tenantList != null && DateTime.UtcNow <= _cacheDirtyTime) return;
            lock (_syncLock)
            {
                if (_tenantList == null || DateTime.UtcNow > _cacheDirtyTime)
                {
                    _tenantLookup.Clear();
                    _uriTenantLookup.Clear();
                    _b2CTenantLookup.Clear();
                    _tenantList = fetchValue();
                    foreach (var tenant in _tenantList)
                    {
                        _tenantLookup[tenant.TenantId] = tenant;
                        if (tenant.AuthType == TenantAuthenticationType.B2C)
                        {
                            _uriTenantLookup[(tenant.Url ?? "").ToLowerInvariant()] = tenant;
                            _b2CTenantLookup[tenant.Name] = tenant;
                        }
                    }
                }
                ResetDirtyCacheTime();
            }
        }

    }
}