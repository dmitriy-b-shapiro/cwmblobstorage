﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Cwm.Data.Azure.Tenant
{
    [XmlRoot(ElementName = "Tenants")]
    public class TenantsSerialization
    {
        public const int CurrentVersion = 3; // Increment this each time we change the Tenant class

        public int Version { get; set; }

        [XmlElement("Tenant")]
        public List<TenantEntity> Tenants { get; set; }
    }
}