﻿using System;
using System.Data;
using System.Data.SqlClient;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Newtonsoft.Json;

namespace Cwm.Data.Azure.Tenant
{
    public class SessionCache : IPersistedCache2020
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SessionCache));
        private readonly ICwmConfig _config;
        private ITenantRepository _tenantRepository;

        public SessionCache(ICwmConfig config)
        {
            _config = config;
        }

        public ITenantRepository TenantRepository
        {
            set => _tenantRepository = value;
        }

        private string Serialize<T>(T data)
        {
            if (data == null) return null;
            var result = JsonConvert.SerializeObject(data);
            return result;
        }

        private T Deserialize<T>(string data)
        {
            if (data == null) return default(T);
            var result = JsonConvert.DeserializeObject<T>(data);
            return result;
        }

        private IDbConnection CreateConnection(IUserSession userSession)
        {
            if (_tenantRepository == null) throw new InvalidOperationException("Assign TenantRepository before using");
            var tenant = TenantsCache.Read(userSession.TenantId, () => _tenantRepository.GetTenants());
            var connStr = _config.ConnectionStrings[tenant.Name];
            var dbConn = new SqlConnection(connStr.ConnectionString);
            return dbConn;
        }

        public void AddOrUpdate(IUserSession userSession, string key, object value, int expiresInSeconds = 300, bool sliding = true)
        {
            try
            {

                using (var conn = CreateConnection(userSession))
                {
                    conn.Open();
                    try
                    {
                        RealAddOrUpdate(conn, key, value, expiresInSeconds, sliding);
                    }
                    catch (Exception ex)
                    {
                        Log.Info("Failed to Add or Update from DB cache. Will try again: tenant = " + userSession.TenantId + " key=" + key, ex);
                        //give it one more try
                        RealAddOrUpdate(conn, key, value, expiresInSeconds, sliding);
                    }
                }                 
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Add or Update from DB cache, tenant = " + userSession.TenantId + " key=" + key, ex);
                throw;
            }
        }

        private void RealAddOrUpdate(IDbConnection conn, string key, object value,
            int expiresInSeconds = 300,
            bool sliding = true)
        {
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT 1 FROM SessionCacheItems WHERE [Key] = @Key";
                cmd.AddParameter("Key", key);
                using (var trans = conn.BeginTransaction())
                {
                    cmd.Transaction = trans;
                    try
                    {
                        var exists = Convert.ToInt32(cmd.ExecuteScalar()) == 1;
                        cmd.AddParameter("Value", Serialize(value));
                        cmd.AddParameter("Now", DateTime.UtcNow);
                        if (exists)
                        {
                            cmd.CommandText =
                                @"UPDATE SessionCacheItems SET Value = @Value, ModifiedUtc = @Now WHERE [Key] = @Key";
                        }
                        else
                        {
                            cmd.CommandText = @"INSERT INTO SessionCacheItems
([Key], Value, ModifiedUtc, ExpirationSeconds, Sliding, CreatedUtc) 
VALUES 
(@Key, @Value, @Now, @ExpirationSeconds, @Sliding, @Now)";
                            cmd.AddParameter("ExpirationSeconds", expiresInSeconds);
                            cmd.AddParameter("Sliding", sliding);
                        }

                        cmd.ExecuteNonQuery();
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw;
                    }
                }

            }
        }

        public void Clear(IUserSession userSession)
        {
            try
            {
                using (var conn = CreateConnection(userSession))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "DELETE FROM SessionCacheItems WHERE [Key] like @Key";
                        cmd.AddParameter("Key", userSession.UserId + "|%");
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Remove from DB cache, tenant = " + userSession.TenantId + " User =" + userSession.UserId, ex);
                throw;
            }
        }

        public void Remove(IUserSession userSession, string key)
        {
            try
            {
                using (var conn = CreateConnection(userSession))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "DELETE FROM SessionCacheItems WHERE [Key] = @Key";
                        cmd.AddParameter("Key", key);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Remove from DB cache, tenant = " + userSession.TenantId + " key=" + key, ex);
                throw;
            }
        }

        public T Get<T>(IUserSession userSession, string key) where T : class
        {
            string dbData = null;
            try
            {
                using (var conn = CreateConnection(userSession))
                {
                    conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT Value, Sliding FROM SessionCacheItems WHERE [Key] = @Key AND DATEADD(ss,ExpirationSeconds,ModifiedUtc) > @Now";
                        cmd.AddParameter("Key", key);
                        cmd.AddParameter("Now", DateTime.UtcNow);
                        bool updateTime = false;
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                dbData = reader["Value"].ToString();
                                updateTime = Convert.ToBoolean(reader["Sliding"]);
                            }
                        }
                        using (var trans = conn.BeginTransaction())
                        {
                            cmd.Transaction = trans;
                            try
                            {
                                if (updateTime)
                                {
                                    cmd.CommandText =
                                        @"UPDATE SessionCacheItems SET ModifiedUtc = @Now WHERE [Key] = @Key";
                                    cmd.ExecuteNonQuery();
                                }
                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                Log.Error("Failed to update time in DB cache, tenant = " + userSession.TenantId + " key=" + key, ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Get from DB cache, tenant = " + userSession.TenantId + " key=" + key, ex);
                throw;
            }
            
            return Deserialize<T>(dbData);
        }

        public T GetOrAdd<T>(IUserSession userSession, string key, Func<T> func, int expiresInSeconds = 300, bool sliding = true) where T : class
        {
            T data;
            try
            {
                using (var conn = CreateConnection(userSession))
                {
                    conn.Open();
                    try
                    {
                        data = RealGetOrAdd(conn, key, func, expiresInSeconds, sliding);
                    }
                    catch (Exception ex)
                    {
                        Log.Info("Failed to Get from DB cache, will try again. Tenant = " + userSession.TenantId + " key=" + key, ex);
                        data = RealGetOrAdd(conn, key, func, expiresInSeconds, sliding);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Get from DB cache, tenant = " + userSession.TenantId + " key=" + key, ex);
                throw;
            }

            return data;
        }

        public T  RealGetOrAdd<T>(IDbConnection conn, string key, Func<T> func, int expiresInSeconds,
            bool sliding) where T : class
        {
            string dbData = null;
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText =
                    "SELECT Value, Sliding, ExpirationSeconds, ModifiedUtc FROM SessionCacheItems WHERE [Key] = @Key";
                cmd.AddParameter("Key", key);
                cmd.AddParameter("Now", DateTime.UtcNow);
                using (var trans = conn.BeginTransaction())
                {
                    cmd.Transaction = trans;
                    try
                    {
                        bool updateTime = false;
                        bool found = false;
                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                found = true;
                                dbData = reader["Value"].ToString();
                                updateTime = Convert.ToBoolean(reader["Sliding"]);
                                if (Convert.ToDateTime(reader["ModifiedUtc"])
                                        .AddSeconds(Convert.ToInt32(reader["ExpirationSeconds"])) >
                                    DateTime.UtcNow)
                                {
                                    //since it has expired, we want to ignore the return and not update the time.
                                    dbData = null;
                                    updateTime = false;
                                }
                            }
                        }

                        if (found)
                        {
                            if (updateTime)
                            {
                                cmd.CommandText =
                                    @"UPDATE SessionCacheItems SET ModifiedUtc = @Now WHERE [Key] = @Key";
                                cmd.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            cmd.CommandText = @"INSERT INTO SessionCacheItems
([Key], Value, ModifiedUtc, ExpirationSeconds, Sliding, CreatedUtc) 
VALUES 
(@Key, @Value, @Now, @ExpirationSeconds, @Sliding, @Now)";
                            cmd.AddParameter("Value", Serialize(func()));
                            cmd.AddParameter("ExpirationSeconds", expiresInSeconds);
                            cmd.AddParameter("Sliding", sliding);
                            cmd.ExecuteNonQuery();

                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw;
                    }
                }

            }

            return Deserialize<T>(dbData);
        }

    }

    public static class DbCommandExtensions
    {
        private static IDbDataParameter AddParameter(this IDbCommand command, string name)
        {
            if (-1 != command.Parameters.IndexOf(name))
            {
                command.Parameters.RemoveAt(name);
            }

            var param = command.CreateParameter();
            param.Direction = ParameterDirection.Input;
            param.ParameterName = name;
            command.Parameters.Add(param);
            return param;
        }
        public static void AddParameter(this IDbCommand command, string name, string value)
        {
            var param = AddParameter(command, name);
            param.Value = string.IsNullOrEmpty(value) ? string.Empty : value;
        }

        /// <summary>
        /// Adds the parameter to the command
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="name">Parameter name</param>
        /// <param name="value">Parameter value</param>
        public static void AddParameter(this IDbCommand command, string name, DateTime value)
        {
            var param = AddParameter(command, name);
            if (value == DateTime.MinValue)
            {
                param.Value = "";
            }
            else
            {
                param.Value = value;
            }
        }

        /// <summary>
        /// Adds the parameter to the command
        /// </summary>
        /// <example>
        /// </example>
        /// <param name="command">Command</param>
        /// <param name="name">Parameter name</param>
        /// <param name="value">Parameter value</param>
        public static void AddParameter(this IDbCommand command, string name, Object value)
        {
            AddParameter(command, name).Value = value;
        }


    }
}