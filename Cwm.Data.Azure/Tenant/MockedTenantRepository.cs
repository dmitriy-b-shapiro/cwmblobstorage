﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Cwm.Common;
using Cwm.Common.Interfaces;
using Cwm.Common.Models;

namespace Cwm.Data.Azure.Tenant
{
    internal class MockedTenantRepository : ITenantRepository
    {
        private readonly List<Common.Models.Tenant> _allowedTenants;
        private readonly HashSet<string> _allowedAuthTypes = new HashSet<string>();

        public MockedTenantRepository(ICwmConfig config)
        {
            TenantRepository.PopulateAllowedAuthTypes(config, _allowedAuthTypes);

            var knownConnections = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (ConnectionStringSettings connectionString in config.ConnectionStrings)
            {
                if (connectionString.ProviderName != "System.Data.SqlClient") continue;
                knownConnections.Add(connectionString.Name);
            }

            // use the embedded resource
            var tenantsSerialization = Utilities.DeserializeFromXmlString<TenantsSerialization>(Utilities.Extract("Cwm.Data.Azure.Tenant.MockedTenants.xml"));
            _allowedTenants = tenantsSerialization.Tenants
                .Where(t =>
                    _allowedAuthTypes.Contains(t.AuthType)
                    && knownConnections.Contains(t.Name))
                .Select(t => t.ToTenant()).ToList();
        }

        public List<Common.Models.Tenant> GetTenants()
        {
            return _allowedTenants;
        }

        public Common.Models.Tenant Read(string tenantId)
        {
            return _allowedTenants.Find(t => t.TenantId == tenantId);
        }

        public void Add(Common.Models.Tenant tenant)
        {
            // no-op
        }

        public void Remove(Common.Models.Tenant tenant)
        {
            throw new NotSupportedException();
        }

        public void Update(Common.Models.Tenant tenant)
        {
            throw new NotSupportedException();
        }

       
    }
}