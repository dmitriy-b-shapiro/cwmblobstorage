using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Cwm.Common.Models;
using Humanizer;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cwm.Data.Azure.Tenant
{
    /// <summary>
    /// TenantRepository.
    /// Note this class is not thread safe. We rely on the DI container creating a new instance on every request.
    /// </summary>
    internal class TenantRepository : ITenantRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(TenantRepository));
        private readonly ICwmConfig _config;
        private readonly string _tableName;
        private CloudTable _table;
        private readonly HashSet<string> _allowedAuthTypes = new HashSet<string>();
        private readonly HashSet<string> _knownConnections = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

        public TenantRepository(ICwmConfig config)
        {
            _config = config;
            _tableName = TenantTableAndEntityStrings.TableName;

            PopulateAllowedAuthTypes(config, _allowedAuthTypes);
            //Here to support unit tests
            if (_config.ConnectionStrings == null)
            {
                _knownConnections.Add("Local1");
                _knownConnections.Add("Local2");
                _knownConnections.Add("stratusSandbox1");
            }
            else
            {
                foreach (ConnectionStringSettings connectionString in _config.ConnectionStrings)
                {
                    if (connectionString.ProviderName != "System.Data.SqlClient") continue;
                    _knownConnections.Add(connectionString.Name);
                }
            }
        }

        public static void PopulateAllowedAuthTypes(ICwmConfig config, HashSet<string> allowedAuthTypes)
        {
            allowedAuthTypes.Clear();
            switch (config.AllowedAuthSchemas)
            {
                case AllowedAuthenticationTypes.All:
                case AllowedAuthenticationTypes.Classic:
                    allowedAuthTypes.Add(TenantAuthenticationType.Default.ToString());
                    allowedAuthTypes.Add(TenantAuthenticationType.B2C.ToString());
                    break;
                case AllowedAuthenticationTypes.B2C:
                    allowedAuthTypes.Add(TenantAuthenticationType.B2C.ToString());
                    break;
            }
        }

        private CloudTable Table
        {
            get
            {
                if (_table != null) return _table;

                var storageAccount = CloudStorageAccount.Parse(_config.StorageConnectionString);

                var tableClient = storageAccount.CreateCloudTableClient();
                _table = tableClient.GetTableReference(_tableName);
                return _table;
            }
        }

        public List<Common.Models.Tenant> GetTenants()
        {
            var query = from entity in Table.CreateQuery<TenantEntity>().Execute()
                where entity.PartitionKey == _config.Environment
                      && _knownConnections.Contains(entity.Name)
                      && _allowedAuthTypes.Contains(entity.AuthType)
                        select entity.ToTenant();

            return query.ToList();
        }

        public Common.Models.Tenant Read(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId)) throw new ArgumentNullException("tenantId");

            var retrieveOperation = TableOperation.Retrieve<TenantEntity>(_config.Environment, tenantId);
            var retrievedResult = Table.Execute(retrieveOperation);

            var tenantEntity = retrievedResult.Result as TenantEntity;
            return tenantEntity != null ? tenantEntity.ToTenant() : null;
        }

        public void Add(Common.Models.Tenant tenant)
        {
            CheckTenantArgument(tenant);
            var tenantThere = Read(tenant.TenantId);
            if (tenantThere != null) throw new Exception("Tenant " + tenant.Name + " already exists");

            var insertOperation = TableOperation.Insert(TenantEntity.FromTenant(tenant));
            var result = Table.Execute(insertOperation);
            if (result.HttpStatusCode != 200) throw new Exception("Failed to add tenant to tenant table. Response code was {0}".FormatWith(result.HttpStatusCode));
        }

        public void Remove(Common.Models.Tenant tenant)
        {
            CheckTenantArgument(tenant);
            var tenantThere = ReadEntity(tenant.TenantId);
            if (tenantThere == null) return;

            var result = Table.Execute(TableOperation.Delete(tenantThere));
            if (result.HttpStatusCode != 200) throw new Exception("Failed to remove tenant from tenant table. Response code was {0}".FormatWith(result.HttpStatusCode));
        }

        public void Update(Common.Models.Tenant tenant)
        {
            CheckTenantArgument(tenant);
            var tenantThere = ReadEntity(tenant.TenantId);
            if (tenantThere == null) throw new Exception("Tenant " + tenant.Name + " not there");

            var tenantEntity = TenantEntity.FromTenant(tenant);
            var retrieveOperation = TableOperation.Retrieve<TenantEntity>(tenantEntity.PartitionKey, tenantEntity.RowKey);
            var retrievedResult = Table.Execute(retrieveOperation);
            var updateEntity = retrievedResult.Result as TenantEntity;
            if (updateEntity == null) throw new Exception("Tenant " + tenant.Name + " from table is not a valid tenant entity.");
            updateEntity.UpdatePropertiesFrom(tenantEntity);
            var updateOperation = TableOperation.Replace(updateEntity);
            var result = Table.Execute(updateOperation);
            if (result.HttpStatusCode != 200) throw new Exception("Failed to update tenant in tenant table. Response code was {0}".FormatWith(result.HttpStatusCode));

            throw new Exception("Tenant " + tenant.Name + " from table is not a valid tenant entity.");
        }
       
        private TenantEntity ReadEntity(string tenantId)
        {
            if (string.IsNullOrEmpty(tenantId)) throw new ArgumentNullException("tenantId");

            var retrieveOperation = TableOperation.Retrieve<TenantEntity>(_config.Environment, tenantId);
            var retrievedResult = Table.Execute(retrieveOperation);

            return retrievedResult.Result as TenantEntity;
        }

        private static void CheckTenantArgument(Common.Models.Tenant tenant)
        {
            if (tenant == null) throw new ArgumentNullException("tenant");
            if (string.IsNullOrEmpty(tenant.Name)) throw new ArgumentNullException("tenant");
        }
    }
}