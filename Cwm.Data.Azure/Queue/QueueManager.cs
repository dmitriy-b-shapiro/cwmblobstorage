﻿using System;
using System.Text.RegularExpressions;
using Cwm.Common.Extensions;
using Cwm.Common.Interfaces;
using Cwm.Common.Queue;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Cwm.Data.Azure.Queue
{
    internal class QueueManager : IQueueManager
    {
        private readonly ICwmConfig _config;
        private readonly CloudQueueClient _queueClient;

        public QueueManager(ICwmConfig config)
        {
            _config = config;

            var storageAccount = CloudStorageAccount.Parse(_config.StorageConnectionString);
            _queueClient = storageAccount.CreateCloudQueueClient();
        }

        public void AddToQueue<T>(string queueName, T item) where T : QueueMessageBase
        {
            if (_config.IsRunningLocally)
            {
                queueName = queueName + "-" + Environment.MachineName.ToBase64().ToLower();
                var rgx = new Regex("[^a-z0-9 -]");
                queueName = rgx.Replace(queueName, "");
            }

            var queue = _queueClient.GetQueueReference(queueName);
            queue.CreateIfNotExists();

            var message = new CloudQueueMessage(JsonConvert.SerializeObject(item));
            queue.AddMessage(message);            
        }
    }
}
