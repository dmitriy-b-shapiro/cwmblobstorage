using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading.Tasks;
using Cwm.Common.Extensions;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Cwm.Common.Models;
using Humanizer;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Azure.ActiveDirectory.GraphClient.Extensions;
using TenantDetail = Cwm.Common.Models.TenantDetail;

namespace Cwm.Data.Azure.Directory
{
    public class AadDirectoryService : IDirectoryService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AadDirectoryService));
        private readonly IUserSession _userSession;
        private readonly ITokenService _tokenService;
        private readonly IMemoryCache _cache;
        private const string GraphApi = "https://graph.windows.net";

        private const string UsersCacheKey = "AADUsers";
        private const string TenantCacheKey = "AADTenant";

        public AadDirectoryService(IUserSession userSession, ITokenService tokenService, IMemoryCache cache)
        {
            _userSession = userSession;
            _tokenService = tokenService;
            _cache = cache;
        }

        public async Task<bool> AddApplicationToAadRole(string appId, string roleName, Uri uri)
        {
            try
            {
                var client = GetAdClientForUser();

                var app = await client.ServicePrincipals.Where(a => a.AppId.Equals(appId, StringComparison.OrdinalIgnoreCase)).ExecuteSingleAsync();
                if (app == null)
                {
                    Log.Error("Failed to find a service principal in AAD with app id " + appId);
                    return false;
                }

                var roles = await client.DirectoryRoles.ExecuteAsync();
                var role = roles.CurrentPage.FirstOrDefault(r => r.DisplayName.Equals(roleName, StringComparison.OrdinalIgnoreCase)) as DirectoryRole;
                if (role == null)
                {
                    Log.Error("Failed to find an AAD role with name " + roleName);
                    return false;
                }

                if (role.Members.All(m => m.ObjectId != app.ObjectId))
                {
                    try
                    {
                        role.Members.Add(app as DirectoryObject);
                        await role.UpdateAsync();
                    }
                    catch (Exception ex)
                    {
                        Log.Warn("Error trying to add application service principal to Admin role", ex);
                    }
                }
                else
                {
                    Log.Info("Service principal {0} was already in role {1}".FormatWith(appId, roleName));
                    return true;
                }

                Log.Info("Successfully assigned service principal {0} to role {1}".FormatWith(appId, roleName));
            }
            catch (Exception ex)
            {
                Log.Error("Failed to assign service principal {0} to role {1}".FormatWith(appId, roleName), ex);
                return false;
            }

            return true;
        }

        public async Task<UserProfilePagedResult> FindUserProfiles(List<string> hiddenUsers, int count = 10, int page = 1, string search = null)
        {
            var client = await GetAdClient();
            IReadOnlyQueryableSet<IUser> query;
            List<UserProfile> users;

            //Log.Info("AD query: BEGIN");
            if (!string.IsNullOrWhiteSpace(search))
            {
                query = client.Users.Where(user =>
                    user.UserPrincipalName.StartsWith(search) ||
                    user.DisplayName.StartsWith(search) ||
                    user.GivenName.StartsWith(search) ||
                    user.Surname.StartsWith(search));
                users = await GetUsers(query);
            }
            else
            {
                users = _cache.Get<List<UserProfile>>(_userSession, UsersCacheKey);
                if (users == null)
                {
                    query = client.Users.OrderBy(user => user.DisplayName);
                    users = await GetUsers(query);
                    _cache.AddOrUpdate(_userSession, UsersCacheKey, users);
                }
            }
            //Log.Info("AD query: END");
            if (hiddenUsers != null) users = users.Where(u => !hiddenUsers.Contains(u.Id)).ToList();

            var result = new UserProfilePagedResult
            {
                HasMorePages = users.Count > (page * count),
                ItemsPerPage = count,
                Page = page,
                Items = users.Skip(((page < 1 ? 1 : page) - 1) * count).Take(count).ToList()
            };

            return result;
        }

        public async Task<UserProfile> GetUserProfile(string userId = null)
        {
            var client = await GetAdClient();

            if (userId == null) userId = _userSession.UserId;
            var user = await client.Users.Where(u => u.ObjectId.Equals(userId)).ExecuteSingleAsync();
            return UserProfileFromDirUser(user);
        }

        public async Task<UserProfile> AddUserProfile(AddUserProfile userProfile, SecureString pwd)
        {
            var user = new User
            {
                AccountEnabled = true,
                DisplayName = userProfile.DisplayName,
                MailNickname = string.IsNullOrWhiteSpace(userProfile.UserPrincipalName) ? "" : userProfile.UserPrincipalName.Split(new[] { '@' })[0],
                PasswordProfile = new PasswordProfile { ForceChangePasswordNextLogin = userProfile.ForceChangePasswordNextLogin, Password = pwd.ConvertToUnsecureString() },
                UserPrincipalName = userProfile.UserPrincipalName,
                City = userProfile.City,
                State = userProfile.State,
                PostalCode = userProfile.Zip,
                Country = userProfile.Country,
                Mail = userProfile.Email,
                GivenName = userProfile.FirstName,
                Surname = userProfile.LastName,
                TelephoneNumber = userProfile.TelephoneNumber,
                StreetAddress = userProfile.StreetAddress
            };
            
            var client = await GetAdClient();

            try
            {
                await client.Users.AddUserAsync(user);
                InvalidateUsersCache();
            }
            catch (Exception ex)
            {
                AadExceptionParser.ProcessException(ex);
            }

            var users = await client.Users
               .Where(u => u.UserPrincipalName.Equals(userProfile.UserPrincipalName))
               .ExecuteAsync();

            return UserProfileFromDirUser(users.CurrentPage[0]);
        }

        public async Task<List<string>> FindGlobalAdmins()
        {
            // this method is currently used by unit tests only

            var client = await GetAdClient();

            IDirectoryRole adminRole;
            var roles = await client.DirectoryRoles.ExecuteAsync();
            do
            {
                adminRole = roles.CurrentPage.FirstOrDefault(r => r.DisplayName.Equals("Company Administrator"));
                if (adminRole != null) break;
                roles = await roles.GetNextPageAsync();

            } while (roles != null && roles.MorePagesAvailable);


            if (adminRole == null) return new List<string>();

            IDirectoryRoleFetcher fetcher = (DirectoryRole)adminRole;
            var members = await fetcher.Members.ExecuteAsync();

            return members.CurrentPage.Select(m => m.ObjectId).ToList();
        }

        public async Task DeleteUserProfile(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentException("userId");

            var client = await GetAdClient();

            var user = await client.Users.GetByObjectId(userId).ExecuteAsync();
            if (user == null) return;

            await user.DeleteAsync();

            InvalidateUsersCache();
        }

        public async Task UpdateUserProfile(UpdateUserProfile userProfile)
        {
            var client = await GetAdClient();

            try
            {
                var user = await client.Users.GetByObjectId(userProfile.Id).ExecuteAsync();
                if (user == null) return;
                if (!string.IsNullOrWhiteSpace(userProfile.UserPrincipalName)) user.UserPrincipalName = userProfile.UserPrincipalName;
                if (!string.IsNullOrWhiteSpace(userProfile.UserPrincipalName)) user.MailNickname = userProfile.UserPrincipalName.Split(new[] { '@' })[0];
                if (!string.IsNullOrWhiteSpace(userProfile.DisplayName)) user.DisplayName = userProfile.DisplayName;
                if (!string.IsNullOrWhiteSpace(userProfile.City)) user.City = userProfile.City;
                if (!string.IsNullOrWhiteSpace(userProfile.State)) user.State = userProfile.State;
                if (!string.IsNullOrWhiteSpace(userProfile.Zip)) user.PostalCode = userProfile.Zip;
                if (!string.IsNullOrWhiteSpace(userProfile.Country)) user.Country = userProfile.Country;
                if (!string.IsNullOrWhiteSpace(userProfile.Email)) user.Mail = userProfile.Email;
                if (!string.IsNullOrWhiteSpace(userProfile.FirstName)) user.GivenName = userProfile.FirstName;
                if (!string.IsNullOrWhiteSpace(userProfile.LastName)) user.Surname = userProfile.LastName;
                if (!string.IsNullOrWhiteSpace(userProfile.TelephoneNumber)) user.TelephoneNumber = userProfile.TelephoneNumber;
                if (!string.IsNullOrWhiteSpace(userProfile.StreetAddress)) user.StreetAddress = userProfile.StreetAddress;
                if (userProfile.IsSignInEnabled != null) user.AccountEnabled = userProfile.IsSignInEnabled.Value;

                await user.UpdateAsync();
                InvalidateUsersCache();
            }
            catch (Exception ex)
            {
                AadExceptionParser.ProcessException(ex);
            }
        }

        public async Task ResetUserPassword(string userId, SecureString newPassword, bool forceChangePasswordNextLogin)
        {
            if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentException("userId");

            var client = await GetAdClient();

            var user = await client.Users.GetByObjectId(userId).ExecuteAsync();
            if (user == null) return;

            user.PasswordProfile = new PasswordProfile
            {
                ForceChangePasswordNextLogin = forceChangePasswordNextLogin,
                Password = newPassword.ConvertToUnsecureString(),

            };
            try
            {
                await user.UpdateAsync();
            }
            catch (Exception ex)
            {
                AadExceptionParser.ProcessException(ex, "Password");
            }
        }

        public async Task<TenantDetail> GetTenantDetail()
        {
            var cacheKey = TenantCacheKey;

            var tenantDetail = _cache.Get<TenantDetail>(_userSession, cacheKey);
            if (tenantDetail != null) return tenantDetail;

            var client = await GetAdClient();

            var tenant = await client.TenantDetails.GetByObjectId(_userSession.TenantId).ExecuteAsync();
            tenantDetail = TenantDetailFromGraph(tenant);
            _cache.AddOrUpdate(_userSession, cacheKey, tenantDetail, 600);

            return tenantDetail;
        }

        private static async Task<List<UserProfile>> GetUsers(IReadOnlyQueryableSet<IUser> query)
        {
            var users = new List<UserProfile>();

            var pagedResult = await query.ExecuteAsync();
            if (pagedResult?.CurrentPage != null) users.AddRange(pagedResult.CurrentPage.Select(UserProfileFromDirUser).ToList());

            while (pagedResult != null && pagedResult.MorePagesAvailable)
            {
                pagedResult = await pagedResult.GetNextPageAsync();
                if (pagedResult?.CurrentPage != null)
                    users.AddRange(pagedResult.CurrentPage.Select(UserProfileFromDirUser).ToList());
            }

            return users;
        }

        private static TenantDetail TenantDetailFromGraph(ITenantDetail tenant)
        {
            const string aadDefaultDomainSuffix = ".onmicrosoft.com";

            var tenantDomains = new List<string>();
            var defaultTenantDomain = "";

            if (tenant.VerifiedDomains != null)
            {
                var aadVerifiedDomains = tenant.VerifiedDomains.ToList();

                if (!aadVerifiedDomains.All(vd => vd.Name.EndsWith(aadDefaultDomainSuffix)))
                    aadVerifiedDomains.RemoveAll(vd => vd.Name.EndsWith(aadDefaultDomainSuffix));

                if (aadVerifiedDomains.Any())
                {
                    var defaultVerifiedDomain = aadVerifiedDomains.FirstOrDefault(vd => vd.@default.HasValue && vd.@default == true);
                    defaultTenantDomain = defaultVerifiedDomain != null
                        ? defaultVerifiedDomain.Name
                        : aadVerifiedDomains.First().Name;
                }

                tenantDomains = aadVerifiedDomains.Select(vd => vd.Name).ToList();
            }

            return new TenantDetail
            {
                Domains = tenantDomains,
                DefaultDomain = defaultTenantDomain,
                Name = tenant.DisplayName,
                Id = tenant.ObjectId
            };
        }

        private static UserProfile UserProfileFromDirUser(IUser dirUser)
        {
            var user = new UserProfile
            {
                Id = dirUser.ObjectId,
                Email = dirUser.Mail,
                FirstName = dirUser.GivenName,
                LastName = dirUser.Surname,
                UserPrincipalName = dirUser.UserPrincipalName,
                City = dirUser.City,
                State = dirUser.State,
                Country = dirUser.Country,
                Zip = dirUser.PostalCode,
                DisplayName = dirUser.DisplayName,
                StreetAddress = dirUser.StreetAddress,
                TelephoneNumber = dirUser.TelephoneNumber,
                IsReadOnly = dirUser.DirSyncEnabled ?? false,
                IsSignInEnabled = dirUser.AccountEnabled ?? false
            };

            return user;
        }

        private void InvalidateUsersCache()
        {
            _cache.Remove(_userSession, UsersCacheKey);
        }

        private ActiveDirectoryClient GetAdClientForUser()
        {
            var graphUri = new Uri(GraphApi);

            //var token = _tokenService.GetUserLevelToken(_userSession, GraphApi);

            // the access code was cached during sign in
            // we need it here to get a graph api token on behalf of the logged in user
            //var cacheKey = TokenService.AccessCodeCacheKey.FormatWith(_userSession.HashKey);
            //var code = _cache.Get<string>(cacheKey);
            //if (string.IsNullOrWhiteSpace(code)) throw new RedirectToAuthException("No access code found for user " + _userSession.UserPrincipalName);

            //var token = _tokenService.GetUserLevelTokenFromAccessCode(_userSession, code, GraphApi, uri);

            var token = !_userSession.IsExternalUser
                ? _tokenService.GetUserLevelToken(_userSession, GraphApi, null)
                : _tokenService.GetExternalUserLevelToken(_userSession, GraphApi, null); ;
            var adClient = new ActiveDirectoryClient(new Uri(graphUri, _userSession.TenantId), () => Task.FromResult(token.Token));
            return adClient;
        }

        private async Task<ActiveDirectoryClient> GetAdClient()
        {
            var graphUri = new Uri(GraphApi);
            var tenantId = _userSession.TenantId;

            var token = _tokenService.GetApplicationLevelToken(_userSession, GraphApi, tenantId);
            var adClient = new ActiveDirectoryClient(new Uri(graphUri, tenantId), () => Task.FromResult(token.Token));

            return await Task.FromResult(adClient);
        }
    }
}