using System;
using Cwm.Common.Exceptions;
using Newtonsoft.Json;

namespace Cwm.Data.Azure.Directory
{
    public class AadResponse
    {
        [JsonProperty("odata.error")]
        public OdataError Error { get; set; }
    }

    public class OdataError
    {
        public string code { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string lang { get; set; }
        public string value { get; set; }
    }

    public class AadExceptionParser
    {
        public static void ProcessException(Exception exception, string property = null)
        {
            if (exception is Microsoft.Data.OData.ODataErrorException)
            {
                var odataErrorException = exception as Microsoft.Data.OData.ODataErrorException;
                if (odataErrorException.Error != null && !string.IsNullOrWhiteSpace(odataErrorException.Error.ErrorCode))
                {
                    switch (odataErrorException.Error.ErrorCode)
                    {
                        case "Request_BadRequest":
                            throw new BadRequestException(odataErrorException.Error.Message);
                        case "Request_ResourceNotFound":
                            throw new NotFoundException("");
                        default:
                            throw new BadRequestException(odataErrorException.Error.Message);
                    }
                }
                throw new BadRequestException(exception.Message);
            }

            if (exception is InvalidOperationException)
            {
                if (exception.InnerException == null || string.IsNullOrWhiteSpace(exception.InnerException.Message))
                    throw new BadRequestException(exception.Message);

                AadResponse response;
                try
                {
                    response = JsonConvert.DeserializeObject<AadResponse>(exception.InnerException.Message);
                    if (response == null) throw new BadRequestException(exception.InnerException.Message);
                }
                catch (Exception ex)
                {
                    throw new BadRequestException(ex.Message);
                }
                switch (response.Error.code)
                {
                    case "Request_BadRequest":
                        throw new BadRequestException(new BadRequestMessage(response.Error.message.value, property ?? "", response.Error.message.value));
                    case "Request_ResourceNotFound":
                        throw new NotFoundException("");
                    default:
                        throw new BadRequestException(exception.InnerException.Message);
                }
            }

            throw exception;
        }
    }
}