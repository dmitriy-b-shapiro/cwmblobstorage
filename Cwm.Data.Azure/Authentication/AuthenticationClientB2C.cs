﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cwm.Common.Extensions;
using Cwm.Common.Interfaces;
using Cwm.Data.Azure.Token;
using Microsoft.Identity.Client;
using Newtonsoft.Json;

namespace Cwm.Data.Azure.Authentication
{
    public class AuthenticationClientB2C
    {
        private readonly IConfidentialClientApplication _client;
        private readonly PrivateClientTokenCache _cache;
        private readonly Common.Models.Tenant _tenant;
        
        public AuthenticationClientB2C(IUserSession userSession, IPersistedCache2020 sessionCache, 
            Common.Models.Tenant tenant, Uri sourceUri)
        {
            _tenant = tenant;
            _cache = new PrivateClientTokenCache(userSession, sessionCache);
            _client = ConfidentialClientApplicationBuilder.Create(tenant.ClientId)
                .WithClientSecret(tenant.DecodedClientKey)
                .WithRedirectUri(tenant.RedirectUrl(sourceUri))
                .WithB2CAuthority(tenant.B2CSignInMetadataAddress(userSession.B2CFlowName))                
                .Build();

        }

        public AuthenticationResult AcquireTokenByAuthorizationCode(string code)
        {
            var result = _client.AcquireTokenByAuthorizationCode(_tenant.B2CScopes(), code).ExecuteAsync().GetAwaiter().GetResult();
            _cache.Add(new B2cAuthResultCache(result));
            //var b2cCache = new B2cAuthResultCache(result);
            //var data = JsonConvert.SerializeObject(b2cCache);
            //_cache.Add(data);
            return result;
        }

        public AuthenticationResult AcquireTokenSilent()
        {
           var fullToken = _cache.Get<B2cAuthResultCache>();
            //var fullToken = JsonConvert.DeserializeObject<B2cAuthResultCache>(_cache.Get<string>());
            if (fullToken != null)
            {
                var newToken = new AuthenticationResult(fullToken.AccessToken ?? fullToken.IdToken, true, fullToken.UniqueId,
                    new DateTimeOffset(DateTime.UtcNow.AddHours(3)),
                    DateTimeOffset.UtcNow.AddHours(3), _tenant.TenantId, fullToken.Account,
                    fullToken.IdToken, fullToken.Scopes, fullToken.CorrelationId);
                return newToken;
            }

            return null;
        }
    }

    public class B2cAuthResultCache
    {
        public B2cAuthResultCache()
        {

        }

        public B2cAuthResultCache(AuthenticationResult fullToken)
        {
            AccessToken = fullToken.AccessToken ?? fullToken.IdToken;
            IdToken = fullToken.IdToken;
            UniqueId = fullToken.UniqueId;
            Scopes = fullToken.Scopes?.ToList();
            CorrelationId = fullToken.CorrelationId;
            Account = new B2CAuthAccount
            {
                Username = fullToken.Account.Username,
                Environment = fullToken.Account.Environment,
                HomeAccountId = fullToken.Account.HomeAccountId,
            };
        }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("unique_id")]
        public string UniqueId { get; set; }
        [JsonProperty("account")]
        public B2CAuthAccount Account { get; set; }
        [JsonProperty("id_token")]
        public string IdToken { get; set; }
        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }
        [JsonProperty("correlation_id")]
        public Guid CorrelationId { get; set; }
    }
    
    public class B2CAuthAccount : IAccount
    {
        public B2CAuthAccount()
        {

        }

        [JsonProperty("username")]
        public string Username { get; set;  }
        [JsonProperty("environment")]
        public string Environment { get; set; }
        [JsonProperty("home_account_identifier")]
        public string HomeAccountIdentifier { get; set; }
        [JsonProperty("home_account_object_id")]
        public string HomeAccountObjectId { get; set; }
        [JsonProperty("home_account_tenant_id")]
        public string HomeAccountTenantId { get; set; }

        [JsonIgnore]
        public AccountId HomeAccountId
        {
            get => new AccountId(HomeAccountIdentifier, HomeAccountObjectId, HomeAccountTenantId);

            set
            {
                HomeAccountIdentifier = value.Identifier;
                HomeAccountObjectId = value.ObjectId;
                HomeAccountTenantId = value.TenantId;
            }
        }
    }


}