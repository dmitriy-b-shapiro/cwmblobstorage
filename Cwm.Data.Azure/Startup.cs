﻿using System.Configuration;
using Autofac;
using Cwm.Data.Azure.Tenant;
using Microsoft.WindowsAzure.Storage;

namespace Cwm.Data.Azure
{
    internal class Startup : IStartable
    {
        public void Start()
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["cwm.Storage"].ConnectionString);
            InitializeTables(storageAccount);
        }

        private static void InitializeTables(CloudStorageAccount storageAccount)
        {
            var tableClient = storageAccount.CreateCloudTableClient();
            var table = tableClient.GetTableReference(TenantTableAndEntityStrings.TableName);
            table.CreateIfNotExists();
        }
    }
}