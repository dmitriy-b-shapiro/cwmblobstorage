using System.Text;
using System.Threading.Tasks;
using Cwm.Common.Encryption;
using Cwm.Common.Interfaces;
using Newtonsoft.Json;
using StackExchange.Redis.Extensions.Core;

namespace Cwm.Data.Azure.Redis
{
    internal class EncryptedJsonSerializer : ISerializer
    {
        private readonly IUserSession _userSession;
        private readonly IKeyFetcher _keyFetcher;

        /// <summary>
        /// Encoding to use to convert string to byte[] and the other way around.
        /// </summary>
        /// <remarks>
        /// StackExchange.Redis uses Encoding.UTF8 to convert strings to bytes,
        /// hence we do same here.
        /// </remarks>
        private static readonly Encoding Encoding = Encoding.UTF8;

        public EncryptedJsonSerializer(IUserSession userSession, IKeyFetcher keyFetcher)
        {
            _userSession = userSession;
            _keyFetcher = keyFetcher;
        }

        public byte[] Serialize(object item)
        {
            var jsonString = JsonConvert.SerializeObject(item);
            return EncryptString(jsonString);
        }

        public async Task<byte[]> SerializeAsync(object item)
        {
            var jsonString = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(item));
            return EncryptString(jsonString);
        }

        public object Deserialize(byte[] serializedObject)
        {
            var jsonString = Decrypt(serializedObject);
            return JsonConvert.DeserializeObject(jsonString, typeof(object));
        }

        public Task<object> DeserializeAsync(byte[] serializedObject)
        {
            return Task.Factory.StartNew(() => Deserialize(serializedObject));
        }

        public T Deserialize<T>(byte[] serializedObject)
        {
            var jsonString = Decrypt(serializedObject);
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public Task<T> DeserializeAsync<T>(byte[] serializedObject)
        {
            return Task.Factory.StartNew(() => Deserialize<T>(serializedObject));
        }

        private byte[] EncryptString(string jsonString)
        {
            if (string.IsNullOrWhiteSpace(_userSession?.TenantId)) return Encoding.GetBytes(jsonString);

            var aes = _keyFetcher.GetKey(_userSession);
            return aes.Encrypt(Encoding.GetBytes(jsonString));
        }

        private string Decrypt(byte[] serializedObject)
        {
            if (string.IsNullOrWhiteSpace(_userSession?.TenantId)) return Encoding.GetString(serializedObject);

            var aes = _keyFetcher.GetKey(_userSession);
            return Encoding.GetString(aes.Decrypt(serializedObject));
        }
    }
}