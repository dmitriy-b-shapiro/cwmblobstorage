using System;
using System.Configuration;
using Cwm.Common;
using Cwm.Common.Caching;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Humanizer;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace Cwm.Data.Azure.Redis
{
    public class RedisApplicationCache : TenantCacheBase, IPersistedCache
    {
        private readonly ICwmConfig _config;
        private readonly IKeyFetcher _keyFetcher;
        private readonly IMemoryCache _memoryCache;
        private ConnectionMultiplexer _connectionMultiplexer;
        private static readonly ILog Log = LogManager.GetLogger(typeof(RedisApplicationCache));

        public RedisApplicationCache(ICwmConfig config, IKeyFetcher keyFetcher, IMemoryCache memoryCache)
        {
            _config = config;
            _keyFetcher = keyFetcher;
            _memoryCache = memoryCache;
        }

        private ConnectionMultiplexer Connection
        {
            get
            {
                if (_connectionMultiplexer != null) return _connectionMultiplexer;

                var connectionString = _config.CacheConnectionString;
                var syncTimeout = ConfigurationManager.AppSettings["cwm.RedisCacheSyncTimeout"];
                _connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString + ",syncTimeout=" + syncTimeout);
                _connectionMultiplexer.IncludeDetailInExceptions = true;
                _connectionMultiplexer.ErrorMessage +=
                    (sender, args) => Log.Error("The Redis server responded with an error message: " + args.Message);

                return _connectionMultiplexer;
            }
        }

        private ICacheClient CacheClient(IUserSession userSession)
        {
            var serializer = new EncryptedJsonSerializer(userSession, _keyFetcher);
            return new StackExchangeRedisCacheClient(Connection, serializer);
        }

        public void AddOrUpdate(IUserSession userSession, string key, object value, int expiresInSeconds = 300, bool sliding = true)
        {
            var tkey = TenantKey(userSession, key);
            try
            {
                var cache = CacheClient(userSession);

                TimeSpan? ts = null;
                if (sliding && expiresInSeconds > 0) ts = TimeSpan.FromSeconds(expiresInSeconds);

                if (ts.HasValue)
                    cache.Add(tkey, value, ts.Value);
                else
                    cache.Add(tkey, value);
                if (ConfigurationManager.AppSettings["cwm.UseMemoryCache"] == Boolean.TrueString)
                {
                    _memoryCache.AddOrUpdate(userSession, key, value, expiresInSeconds > 300 ? 300 : expiresInSeconds,
                        false);
                }
                if (!sliding && expiresInSeconds > 0) cache.Database.ExpireAbsolute(tkey, expiresInSeconds);

            }
            catch (Exception ex)
            {
                Log.Error("Failed to Add to redis cache, key=" + tkey, ex);
                throw;
            }
        }

        public void Remove(IUserSession userSession, string key)
        {
            var tkey = TenantKey(userSession, key);
            try
            {
                var cache = CacheClient(userSession);

                var result = cache.Remove(tkey);
                if (ConfigurationManager.AppSettings["cwm.UseMemoryCache"] == Boolean.TrueString)
                {
                    _memoryCache.Remove(userSession, key);
                }
                Log.Info("Deleting key " + tkey + " resulted in: " + result);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to Remove from redis cache, key=" + tkey, ex);
                throw;
            }
        }

        public T Get<T>(IUserSession userSession, string key) where T : class
        {
            var tkey = TenantKey(userSession, key);

            if (ConfigurationManager.AppSettings["cwm.UseMemoryCache"] == Boolean.TrueString)
            {
                var memResult = _memoryCache.Get<T>(userSession, key);
                if ((memResult != null) && (memResult != default(T))) return memResult;
            }
            var cache = CacheClient(userSession);
            Log.Info("Getting key from Redis {0} for user {1}".FormatWith(tkey, userSession?.HashKey));

            var result = default(T);
            Action action = () => result = cache.Get<T>(tkey);
            RetryLogic.RetryOnException<Exception>(action, 3, ex => Log.Error("Failed to Get string from redis cache. key=" + tkey, ex));
            return result;
        }

        public T GetOrAdd<T>(IUserSession userSession, string key, Func<T> func, int expiresInSeconds = 300, bool sliding = true) where T : class
        {
            try
            {
                var item = Get<T>(userSession, key);
                if (item != null) return item;

                item = func();
                AddOrUpdate(userSession, key, item, expiresInSeconds, sliding);
                return item;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to GetOrAdd from redis cache, key=" + key, ex);
                throw;
            }
        }

        
    }
}