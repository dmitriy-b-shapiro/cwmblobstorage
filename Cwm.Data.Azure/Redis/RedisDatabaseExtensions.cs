﻿using System;
using StackExchange.Redis;

namespace Cwm.Data.Azure.Redis
{
    public static class RedisDatabaseExtensions
    {
        public static void ExpireAbsolute(this IDatabase cache, string key, int expiresInSeconds)
        {
            cache.KeyExpire(key,
                expiresInSeconds > 0 ? DateTime.UtcNow.AddSeconds(expiresInSeconds) : DateTime.UtcNow.AddMinutes(5));
        }
    }
}