using System;

namespace Cwm.Data.Azure.Token
{
    [Serializable]
    public class SignUpState
    {
        public string StateMarker { get; set; }
        public string TenantId { get; set; }
        public string SignedInUserObjectId { get; set; }

        public bool IsValid(string state, string tenantId, string userId)
        {
            return StateMarker.Equals(state, StringComparison.OrdinalIgnoreCase)
                   && TenantId.Equals(tenantId, StringComparison.OrdinalIgnoreCase)
                   && SignedInUserObjectId.Equals(userId, StringComparison.OrdinalIgnoreCase);
        }
    }
}