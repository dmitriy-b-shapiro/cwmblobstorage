using Cwm.Common.Interfaces;
using Humanizer;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using TokenCache = Microsoft.IdentityModel.Clients.ActiveDirectory.TokenCache;
using TokenCacheNotificationArgs = Microsoft.IdentityModel.Clients.ActiveDirectory.TokenCacheNotificationArgs;

namespace Cwm.Data.Azure.Token
{
    public class StratusTokenCache : TokenCache
    {
        private static readonly object FileLock = new object();
        readonly string _cacheId;
        private readonly IUserSession _userSession;
        readonly IPersistedCache _session;

        public StratusTokenCache(IUserSession userSession, IPersistedCache session)
        {
            _cacheId = "{0}:{1}".FormatWith(userSession.UserId, "TokenCache");
            _userSession = userSession;
            _session = session;
            AfterAccess = AfterAccessNotification;
            BeforeAccess = BeforeAccessNotification;
            Load();
        }

        public void Load()
        {
            lock (FileLock)
            {
                Deserialize(_session.Get<byte[]>(_userSession, _cacheId));
            }
        }

        public void Persist()
        {
            // reflect changes in the persistent store
            _session.AddOrUpdate(_userSession, _cacheId, Serialize(), 0);
            // once the write operation took place, restore the HasStateChanged bit to false
            HasStateChanged = false;
        }

        // Empties the persistent store.
        public override void Clear()
        {
            base.Clear();
            _session.Remove(_userSession, _cacheId);
        }

        public override void DeleteItem(TokenCacheItem item)
        {
            base.DeleteItem(item);
            Persist();
        }

        // Triggered right before ADAL needs to access the cache.
        // Reload the cache from the persistent store in case it changed since the last access.
        void BeforeAccessNotification(TokenCacheNotificationArgs args)
        {
            Load();
        }

        // Triggered right after ADAL accessed the cache.
        void AfterAccessNotification(TokenCacheNotificationArgs args)
        {
            // if the access operation resulted in a cache update
            if (HasStateChanged) // && !_userSession.PersistingToken)
            {
                _userSession.PersistingToken = true;
                Persist();
            }
        }
    }


}