﻿using Cwm.Common.Interfaces;
using Humanizer;

namespace Cwm.Data.Azure.Token
{
    public class PrivateClientTokenCache
    {
        //private static readonly object FileLock = new object();
        private readonly string _cacheId;
        private readonly IUserSession _userSession;
        private readonly IPersistedCache2020 _session;

        public PrivateClientTokenCache(IUserSession userSession, IPersistedCache2020 session)
        {
            _cacheId = CacheKey(userSession);
            _userSession = userSession;
            _session = session;
        }

        public static string CacheKey(IUserSession userSession)
        {
            return "{0}|{1}".FormatWith(userSession.UserId, "PrivateTokenCache");
        }

        public void Add<T>(T result) where T : class
        {
            _session.AddOrUpdate(_userSession, _cacheId, result, 1200);
        }

        public T Get<T>() where T : class
        {
            var result = _session.Get<T>(_userSession, _cacheId);
            return result;
        }


        public void Remove()
        {
            _session.Remove(_userSession, _cacheId);
        }
    }
}