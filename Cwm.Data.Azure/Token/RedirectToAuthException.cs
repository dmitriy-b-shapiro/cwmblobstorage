﻿using System;

namespace Cwm.Data.Azure.Token
{
    public class RedirectToAuthException : Exception
    {
        public RedirectToAuthException() { }
        public RedirectToAuthException(string message) : base(message) { }
    }
}