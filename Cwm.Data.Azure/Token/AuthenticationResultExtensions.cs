using System;
using Cwm.Common.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Cwm.Data.Azure.Token
{
    internal static class AuthenticationResultExtensions
    {
        public static AuthToken ToAuthToken(this AuthenticationResult result, string userId, string resourceId)
        {
            return new AuthToken
            {
                Token = result.AccessToken,
                SignedInUser = userId,
                ResourceId = resourceId,
                TenantId = result.TenantId,
                // for an Access Token, the user info isn't filled out
                TokenRequestorUser = result.UserInfo != null ? result.UserInfo.DisplayableId : "",
                AcquiredAt = DateTime.UtcNow,
                Expiration = result.ExpiresOn.AddMinutes(-5),
                RefreshToken = result.RefreshToken
            };
        }

        public static AuthToken ToAuthToken(this Microsoft.Identity.Client.AuthenticationResult result, string userId, string resourceId, string tenantId = null)
        {
            return new AuthToken
            {
                Token = result.AccessToken ?? result.IdToken,
                SignedInUser = userId,
                ResourceId = resourceId,
                TenantId = result.TenantId ?? tenantId,
                // for an Access Token, the user info isn't filled out
                TokenRequestorUser = "",
                AcquiredAt = DateTime.UtcNow,
                Expiration = result.ExpiresOn.Year > 1900 ? result.ExpiresOn.AddMinutes(-5) : DateTimeOffset.MaxValue,
                //RefreshToken = result.RefreshToken
            };
        }
    }
}