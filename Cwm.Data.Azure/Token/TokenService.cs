using System;
using System.Configuration;
using System.Diagnostics;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Cwm.Common.Models;
using Cwm.Data.Azure.Authentication;
using Cwm.Data.Azure.Tenant;
using Cwm.Middleware;
using Humanizer;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using ClientCredential = Microsoft.IdentityModel.Clients.ActiveDirectory.ClientCredential;

namespace Cwm.Data.Azure.Token
{
    public class TokenService : ITokenService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(TokenService));
        private readonly ICwmConfig _cwmConfig;
        private readonly IMemoryCache _memoryCache;
        private readonly ITenantRepository _tenantRepository;
        private IPersistedCache2020 _persistedCache2020;

        private static readonly object UserTokenLock = new object();
        private static readonly object AppTokenLock = new object();

        public IPersistedCache PersistedCache { private get; set; }
        public IPersistedCache2020 PersistedCache2020
        {
            set
            {
                _persistedCache2020 = value;
                _persistedCache2020.TenantRepository = _tenantRepository;
            }
        }

        public TokenService(ICwmConfig cwmConfig, IMemoryCache memoryCache, ITenantRepository tenantRepository)
        {
            _cwmConfig = cwmConfig;
            _memoryCache = memoryCache;
            _tenantRepository = tenantRepository;
        }

        public void ClearUserToken(IUserSession userSession, string resourceId)
        {
            var tokenCache = new StratusTokenCache(userSession, PersistedCache);
            tokenCache.Clear();
        }

        public AuthToken GetUserLevelTokenFromAccessCode(IUserSession userSession, string code, string resourceId, Uri sourceUri)
        {
            lock (UserTokenLock)
            {
                try
                {
                    var tokenCache = new StratusTokenCache(userSession, PersistedCache);
                    var credential = new ClientCredential(_cwmConfig.ClientId, _cwmConfig.ClientKey);
                    var authContext = new AuthenticationContext(_cwmConfig.TenantAuthority(userSession.TenantId), tokenCache);

                    var result = authContext.AcquireTokenByAuthorizationCode(code, sourceUri, credential, resourceId);

                    Log.Info("Got a token using an auth code for {0}. User: {1}".FormatWith(resourceId, userSession.UserPrincipalName));
                    return result.ToAuthToken(userSession.UserId, resourceId);
                }
                catch (Exception ex)
                {
                    Log.Error("Failed to get token using an auth code for {0}. User: {1}".FormatWith(userSession.UserId, userSession.UserPrincipalName), ex);
                    throw new RedirectToAuthException(ex.Message);
                }
            }
        }

        public AuthToken GetExternalUserLevelTokenFromAccessCode(IUserSession userSession, string code,
            string resourceId, Uri sourceUri,
            string activationCode)
        {
            try
            {
                var tenant = TenantsCache.Read(userSession.TenantId, _tenantRepository.GetTenants);
                var client = new AuthenticationClientB2C(userSession, _persistedCache2020, tenant, sourceUri);

                var result = client.AcquireTokenByAuthorizationCode(code);
                _persistedCache2020.SetActivationCode(userSession, activationCode, _cwmConfig.MinutesToCacheExternalUserCredentials * 60);

                Log.Info("Got a token using an auth code for {0}. External User: {1}".FormatWith(resourceId,
                    userSession.UserPrincipalName));
                return result.ToAuthToken(userSession.UserId, resourceId, userSession.TenantId);
            }
            catch (Exception ex)
            {
                Log.Error(
                    "Failed to get token using an auth code for {0}. Patient User: {1}".FormatWith(userSession.UserId,
                        userSession.UserPrincipalName), ex);
                throw new RedirectToAuthException(ex.Message);
            }
        }

        public AuthToken GetUserLevelToken(IUserSession userSession, string resourceId, Uri sourceUri)
        {
            //lock (UserTokenLock)
            {
                try
                {
                    var tokenCache = new StratusTokenCache(userSession, PersistedCache);

                    var credential = new ClientCredential(_cwmConfig.ClientId, _cwmConfig.ClientKey);
                    var authContext = new AuthenticationContext(_cwmConfig.TenantAuthority(userSession.TenantId),
                        tokenCache);

                    var result = authContext.AcquireTokenSilent(resourceId, credential, UserIdentifier.AnyUser);
                    return result.ToAuthToken(userSession.UserId, resourceId);
                }
                catch (Exception ex)
                {
                    Log.Info(
                        "Failed to get token for {0}. It was probably expired or invalidated - for user {1}".FormatWith(
                            userSession.HashKey, userSession.UserPrincipalName), ex);
                    throw new RedirectToAuthException(ex.Message);
                }
            }
        }
        public AuthToken GetExternalUserLevelToken(IUserSession userSession, string resourceId, Uri sourceUri)
        {
            try
            {
                var timer = new DiagnosticTimer();
                var tenant = TenantsCache.Read(userSession.TenantId, _tenantRepository.GetTenants);
                timer.Mark(1);
                var client = new AuthenticationClientB2C(userSession, _persistedCache2020, tenant, sourceUri);
                timer.Mark(2);
                var result = client.AcquireTokenSilent();
                timer.MarkAndStop(3);
                if (ConfigurationManager.AppSettings["cwm.VerboseDebugLogging"] == bool.TrueString)
                {
                    Log.Info($"Get UserLevelToken From Cache. Read Tenants took {timer.GetElapsed(1)} ms. Creating client {timer.GetElapsed(2,1)} ms. Acquired {timer.GetElapsed(3, 2)} ms");
                }

                if (result == null)
                {
                    var msg = "Failed to get token for {0}. It was probably expired or invalidated - for user {1}"
                        .FormatWith(userSession.HashKey, userSession.UserPrincipalName);
                    Log.Info(msg);
                    throw new RedirectToAuthException(msg);
                }
                return result.ToAuthToken(userSession.UserId, resourceId, userSession.TenantId);
            }
            catch (Exception ex)
            {
                Log.Info("Failed to get token for {0}. It was probably expired or invalidated - for user {1}".FormatWith(userSession.HashKey, userSession.UserPrincipalName), ex);
                throw new RedirectToAuthException(ex.Message);
            }
        }

        public AuthToken GetApplicationLevelToken(IUserSession userSession, string resourceId, string tenantId)
        {
            
            var messageUserSession = new FixedUserSession { UserId = _cwmConfig.ClientId, TenantId = tenantId, PersistingToken = userSession.PersistingToken };

            var token = _memoryCache.Get<AuthToken>(messageUserSession, resourceId);
            if (token != null && !token.IsExpired()) return token;

            lock (AppTokenLock)
            {
                try
                {
                    var credential = new ClientCredential(_cwmConfig.ClientId, _cwmConfig.ClientKey);
                    var authContext = new AuthenticationContext(_cwmConfig.TenantAuthority(tenantId), null);

                    var result = authContext.AcquireToken(resourceId, credential);
                    token = result.ToAuthToken(_cwmConfig.ClientId, resourceId);

                    _memoryCache.AddOrUpdate(messageUserSession, resourceId, token, token.ExpiresInSeconds(), false);
                    return token;
                }
                catch (Exception ex)
                {
                    Log.Error("{0}: Failed to get an application token for {1}.".FormatWith(userSession.HashKey, resourceId), ex);
                    throw new RedirectToAuthException(ex.Message);
                }
            }
        }
    }
}