﻿using Autofac;
using Cwm.Common.Interfaces;
using Cwm.Data.Azure.KeyVault;
using Cwm.Data.Azure.Tenant;
using Cwm.Data.Azure.Token;

namespace Cwm.Data.Azure.Ioc
{
    public class WebJobDataAzureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TenantRepository>().As<ITenantRepository>();
            builder.RegisterType<KeyVaultClient>().As<IKeyVaultClient>();
            builder.RegisterType<TokenService>().As<ITokenService>();
            builder.RegisterType<KeyFetcher>().As<IKeyFetcher>();
        }
    }
}