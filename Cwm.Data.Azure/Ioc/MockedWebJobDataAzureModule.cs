﻿using Autofac;
using Cwm.Common.Interfaces;
using Cwm.Data.Azure.KeyVault;
using Cwm.Data.Azure.Tenant;
using Cwm.Data.Azure.Token;

namespace Cwm.Data.Azure.Ioc
{
    public class MockedWebJobDataAzureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MockedKeyVaultClient>().As<IKeyVaultClient>();
            builder.RegisterType<MockedTenantRepository>().As<ITenantRepository>();
            builder.RegisterType<TokenService>().As<ITokenService>();
            builder.RegisterType<KeyFetcher>().As<IKeyFetcher>();
        }
    }
}