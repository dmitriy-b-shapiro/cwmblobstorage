﻿using Autofac;
using Cwm.Common.Caching;
using Cwm.Common.Interfaces;
using Cwm.Data.Azure.Blob;
using Cwm.Data.Azure.Directory;
using Cwm.Data.Azure.KeyVault;
using Cwm.Data.Azure.Queue;
using Cwm.Data.Azure.Redis;
using Cwm.Data.Azure.Tenant;
using Cwm.Data.Azure.Token;

namespace Cwm.Data.Azure.Ioc
{
    public class DataAzureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QueueManager>().As<IQueueManager>().SingleInstance();
            builder.RegisterType<TenantRepository>().As<ITenantRepository>();
            builder.RegisterType<KeyVaultClient>().As<IKeyVaultClient>();
            builder.RegisterType<TokenService>().As<ITokenService>().SingleInstance();
            builder.RegisterType<AadDirectoryService>().As<IDirectoryService>();
            builder.RegisterType<KeyFetcher>().As<IKeyFetcher>();
            builder.RegisterType<Startup>()
                .As<IStartable>()
                .SingleInstance()
                ;
        }
    }
}
