﻿using Autofac;
using Cwm.Common.Interfaces;
using Cwm.Data.Azure.Blob;
using Cwm.Data.Azure.Directory;
using Cwm.Data.Azure.KeyVault;
using Cwm.Data.Azure.Queue;
using Cwm.Data.Azure.Tenant;
using Cwm.Data.Azure.Token;

namespace Cwm.Data.Azure.Ioc
{
    public class MockedDataAzureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QueueManager>().As<IQueueManager>().SingleInstance();
            builder.RegisterType<MockedTenantRepository>().As<ITenantRepository>();
            builder.RegisterType<MockedKeyVaultClient>().As<IKeyVaultClient>();
            builder.RegisterType<TokenService>().As<ITokenService>().SingleInstance();
            builder.RegisterType<AadDirectoryService>().As<IDirectoryService>();
            builder.RegisterType<KeyFetcher>().As<IKeyFetcher>();
            //builder.RegisterType<BlobRepository>().As<IBlobRepository>();
        }
    }
}