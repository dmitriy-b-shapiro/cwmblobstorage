using System;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;
using AzureEncryptionExtensions;
using AzureEncryptionExtensions.Providers;
using Cwm.Common.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Cwm.Data.Azure.Blob
{
    public class BlobRepository : IBlobRepository
    {
        private const string CacheKey = "blobContainer";
        private readonly IUserSession _userSession;
        private readonly IKeyFetcher _keyFetcher;
        private readonly IMemoryCache _cache;
        private readonly CloudStorageAccount _storageAccount;

        public BlobRepository(IUserSession userSession, ICwmConfig config, IKeyFetcher keyFetcher, IMemoryCache cache)
        {
            _userSession = userSession;
            _keyFetcher = keyFetcher;
            _cache = cache;
            _storageAccount = CloudStorageAccount.Parse(config.StorageConnectionString);
        }

        private CloudBlockBlob GetBlockBlobReference(string tenantId, string name,
            IUserSession userSessionOverride = null)
        {
            var container = _cache.GetOrAdd(userSessionOverride ?? _userSession, CacheKey, () =>
            {
                var blobClient = _storageAccount.CreateCloudBlobClient();
                var blobContainer = blobClient.GetContainerReference(tenantId.ToLower());
                blobContainer.CreateIfNotExists();
                return blobContainer;
            });

            return container.GetBlockBlobReference(name);
        }

        public async Task GetDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var certificate = _keyFetcher.GetKey(userSession);
            var provider = new SymmetricBlobCryptoProvider(certificate.Key);

            var blockBlob = GetBlockBlobReference(userSession.TenantId, id.ToString().ToLower());
            await blockBlob.DownloadToStreamEncryptedAsync(provider, stream);
        }

        public async Task SaveDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var certificate = _keyFetcher.GetKey(userSession);
            var provider = new SymmetricBlobCryptoProvider(certificate.Key);

            var blockBlob = GetBlockBlobReference(userSession.TenantId, id.ToString().ToLower());
            blockBlob.UploadFromStreamEncrypted(provider, stream);

            await Task.CompletedTask;
        }

        public async Task DeleteDocumentAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(userSession.TenantId, id.ToString().ToLower(), userSession);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task<string> GetDocumentUnencryptedAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(userSession.TenantId, id.ToString().ToLower(), userSession);
            return await blockBlob.DownloadTextAsync();
        }


        public async Task SaveDataUnencryptedAsync(IUserSession userSession, string uniqueId, string data)
        {
            var blockBlob = GetBlockBlobReference(userSession.TenantId, uniqueId.ToLower(), userSession);
            await blockBlob.UploadTextAsync(data);
        }

        public async Task<(byte[] data, string filename)> GetFileAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(userSession.TenantId, id.ToString().ToLower(), userSession);

            await blockBlob.FetchAttributesAsync();
            var filename = blockBlob.Metadata["filename"];
            var maxSize = Convert.ToInt32(blockBlob.Metadata["size"]);
            var data = new byte[maxSize];
            await blockBlob.DownloadToByteArrayAsync(data, 0);

            return (data, filename);

        }


        public async Task SaveFileAsync(IUserSession userSession, string uniqueId, string filename, byte[] data)
        {
            int size = data.Length;
            var blockBlob = GetBlockBlobReference(userSession.TenantId, uniqueId.ToLower(), userSession);
            blockBlob.Metadata["filename"] = filename;
            blockBlob.Metadata["size"] = size.ToString();
            await blockBlob.UploadFromByteArrayAsync(data, 0, size);
        }

    }
}