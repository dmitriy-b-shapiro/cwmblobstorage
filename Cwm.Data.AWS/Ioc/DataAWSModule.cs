﻿using Autofac;
using Cwm.Common.Interfaces;
using Cwm.Data.AWS.Blob;

namespace Cwm.Data.AWS.Ioc
{
    /// <summary>
    /// Registers DI services for AWS
    /// </summary>
    public class DataAWSModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

        }
    }
}
