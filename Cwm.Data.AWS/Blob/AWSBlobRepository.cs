﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Extensions.S3.Encryption;
using Amazon.Extensions.S3.Encryption.Primitives;
using Amazon.KeyManagementService;
using Amazon.S3;
using Amazon.S3.Model;
using Cwm.Common.Interfaces;

namespace Cwm.Data.AWS.Blob
{
    public class AWSBlobRepository : IBlobRepository
    {
        private readonly ICwmConfig _config;

        public AWSBlobRepository(IUserSession userSession, ICwmConfig config, IKeyFetcher keyFetcher, IMemoryCache cache)
        {
            _config = config;
        }

        /// <summary>
        /// Descrypts and returns the document stored in AWS S3 Bucket
        /// </summary>
        /// <param name="userSession"></param>
        /// <param name="id"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public async Task GetDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var s3Client = GetS3EncryptedClient();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);
            GetObjectResponse objData = await s3Client.GetObjectAsync(
                new GetObjectRequest()
                {
                    BucketName = userSession.TenantId.ToLower(),
                    Key = id.ToString().ToLower()
                });

            await objData.ResponseStream.CopyToAsync(stream);
        }

        /// <summary>
        /// Encrypts and saves the document in S3 Bucket 
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        /// <param name="stream">Object data</param>
        /// <returns></returns>
        public async Task SaveDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var s3Client = GetS3EncryptedClient();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = userSession.TenantId.ToLower(),
                Key = id.ToString().ToLower(),
                InputStream = stream
                 
            };
            await s3Client.PutObjectAsync(request);
        }

        /// <summary>
        /// Removes the document from S3 Bucket
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        /// <returns></returns>
        public async Task DeleteDocumentAsync(IUserSession userSession, Guid id)
        {
            await CreateBucketIfNotExistsAsync (userSession.TenantId);
            var s3Client = new AmazonS3Client();
            DeleteObjectRequest request = new DeleteObjectRequest()
            {
                BucketName = userSession.TenantId.ToLower(),
                Key = id.ToString().ToLower()
            };
            await s3Client.DeleteObjectAsync(request);
        }

        /// <summary>
        /// Returns document from S3 Bucket
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        /// <returns></returns>
        public async Task<string> GetDocumentUnencryptedAsync(IUserSession userSession, Guid id)
        {
            var s3Client = new AmazonS3Client();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);
            GetObjectResponse objData = await s3Client.GetObjectAsync(
                new GetObjectRequest()
                {
                    BucketName = userSession.TenantId.ToLower(),
                    Key = id.ToString().ToLower()
                });

            StreamReader reader = new StreamReader(objData.ResponseStream);
            string content = reader.ReadToEnd();
            return content;
        }

        /// <summary>
        /// Saves the document in S3 Bucket 
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        /// <param name="stream">Object data</param>
        /// <returns></returns>
        public async Task SaveDataUnencryptedAsync(IUserSession userSession, string uniqueId, string data)
        {
            var s3Client = new AmazonS3Client();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = userSession.TenantId.ToLower(),
                Key = uniqueId,
                ContentBody = data

            };
            await s3Client.PutObjectAsync(request);
        }

        /// <summary>
        /// Returns a file stored in S3 Bucket. Reads file name from object's metadata
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        /// <returns></returns>
        public async Task<(byte[] data, string filename)> GetFileAsync(IUserSession userSession, Guid id)
        {
            var s3Client = new AmazonS3Client();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);
            GetObjectResponse objData = await s3Client.GetObjectAsync(
                new GetObjectRequest()
                {
                    BucketName = userSession.TenantId.ToLower(),
                    Key = id.ToString().ToLower()
                });

            var filename = objData.Metadata["filename"];

            MemoryStream ms = new MemoryStream();
            objData.ResponseStream.CopyTo(ms);
            var data = ms.ToArray();

            return (data, filename);
        }

        /// <summary>
        /// Saves a file in S3 Bucket. Set metadata key with file name
        /// </summary>
        /// <param name="userSession">Provides TenantId to use for bucket name</param>
        /// <param name="id">Stored Object Key Value</param>
        public async Task SaveFileAsync(IUserSession userSession, string uniqueId, string filename, byte[] data)
        {
            var s3Client = new AmazonS3Client();
            await CreateBucketIfNotExistsAsync(userSession.TenantId);

            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = userSession.TenantId.ToLower(),
                Key = uniqueId,
                InputStream = new MemoryStream(data),
            };
            request.Metadata.Add("filename", filename);

            await s3Client.PutObjectAsync(request);
        }

        #region private
        /// <summary>
        /// Creates S3 Bucket is not found
        /// </summary>
        /// <param name="tenantId">bucket name</param>
        /// <returns></returns>
        private async Task CreateBucketIfNotExistsAsync(string tenantId)
        {
            try
            {
                var s3Client = new AmazonS3Client();

                var getBucketLocationResponse = await s3Client.ListBucketsAsync();
                if (!getBucketLocationResponse.Buckets.Any(
                        bucket => bucket.BucketName == tenantId.ToLower()))
                {
                    // Add new S3 Bucket
                    await s3Client.PutBucketAsync(tenantId.ToLower());
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }

        /// <summary>
        /// Returns S3Client for documents that require encryption/descryption
        /// </summary>
        /// <returns></returns>
        private AmazonS3EncryptionClientV2 GetS3EncryptedClient()
        {
            var encryptionContext = new Dictionary<string, string>();
            var encryptionMaterial = new EncryptionMaterialsV2(_config.KmsKeyId, KmsType.KmsContext, encryptionContext);
            var configuration = new AmazonS3CryptoConfigurationV2(SecurityProfile.V2AndLegacy)
            {
                StorageMode = CryptoStorageMode.ObjectMetadata
            };
            var s3Client = new AmazonS3EncryptionClientV2(configuration, encryptionMaterial);
            return s3Client;
        }
        #endregion private

    }
}