﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Cwm.Common.Extensions
{
    /// <summary>
    /// Helper class for Enum
    /// </summary>
    public static class EnumExtensionMethod
    {
        /// <summary>
        /// Get Description of enum Fields
        /// </summary>
        /// <param name="enumValue">Type of Enum</param>
        /// <returns></returns>
        public static string GetDescription(this Enum enumValue)
        {
            FieldInfo fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();

        }
    }
}
