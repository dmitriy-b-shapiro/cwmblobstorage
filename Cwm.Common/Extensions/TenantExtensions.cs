﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Cwm.Common.Models;

namespace Cwm.Common.Extensions
{
    public static class TenantExtensions
    {
        public const string FlowNameSignup = "signup";
        public const string FlowNamePassword = "pwd";
        public const string FlowNameAccount = "account";

        public static string AuthInstanceName(this Tenant tenant)
        {
            if (tenant == null) return "";
            switch (tenant.AuthType)
            {
                case TenantAuthenticationType.B2C:
                    int index = tenant.ResourceName.IndexOf(".", StringComparison.Ordinal);
                    return (index >= 0 ? tenant.ResourceName.Substring(0, index) : tenant.ResourceName) + ".b2clogin.com";
                default:
                    return tenant.ResourceName;
            }
        }

        public static string B2CSignInMetadataAddress(this Tenant tenant, string flowName)
        {

            var result = $"https://{tenant.AuthInstanceName()}/tfp/{tenant.ResourceName}/{flowName}/v2.0/.well-known/openid-configuration";

            return result;
        }
        public static string GetFlowName(this Tenant tenant, string flowType)
        {
            var flowName = tenant.Flows.Signup;
            switch (flowType)
            {
                case FlowNamePassword:
                    flowName = tenant.Flows.Password;
                    break;
                case FlowNameAccount:
                    flowName = tenant.Flows.Account;
                    break;
            }

            return flowName;
        }

       
        public static string B2CIdentityName(this Tenant tenant)
        {
            var result = $"https://{tenant.AuthInstanceName()}/{tenant.TenantId}/v2.0/";

            return result;
        }

        public static string RedirectUrl(this Tenant tenant, Uri sourceUri)
        {
            
            switch (tenant.AuthType)
            {
                case TenantAuthenticationType.B2C:
                    return $"{sourceUri.Scheme}://{sourceUri.Authority}/Patient";
            }

            return $"{sourceUri.Scheme}://{sourceUri.Host}";
        }


        public static IEnumerable<string> B2CScopes(this Tenant tenant)
        {
            if (string.IsNullOrWhiteSpace(tenant.AuthScope)) return new string[0];

            var scopes = tenant.AuthScope.Split(';').Select(scope => $"https://{tenant.ResourceName}/api/{scope}");

            return scopes;
        }

    }
}