﻿using System;
using System.Linq;
using System.Security.Claims;
using DocumentFormat.OpenXml.Vml.Spreadsheet;

namespace Cwm.Common.Extensions
{
    public static class ClaimsIdentityExtensions
    {
        private const string ClaimTypeFirstName = "extension_Firstname";
        private const string ClaimTypeLastName = "extension_Lastname";
        private const string ClaimTypeB2cProvider = "tfp";
        private const string ClaimTypeEmails = "emails";
        private const string ClaimTypeObjectId = "http://schemas.microsoft.com/identity/claims/objectidentifier";
        private const string ClaimTypeTenantId = "http://schemas.microsoft.com/identity/claims/tenantid";
        private const string ClaimTypeScope = "http://schemas.microsoft.com/identity/claims/scope";
        private const string ClaimTypeIdentityProvider = "http://schemas.microsoft.com/identity/claims/identityprovider";

        public static string SafeFindFirst(this ClaimsIdentity claimsIdentity, params string[] types)
        {
            Claim claim = null;
            foreach (var type in types)
            {
                claim = claimsIdentity.FindFirst(type);
                if (claim != null) break;
            }
            
            return claim != null ? claim.Value : string.Empty;
        }

        public static string SafeFindFirst(this ClaimsPrincipal claimsIdentity, params string[] types)
        {
            Claim claim = null;
            foreach (var type in types)
            {
                claim = claimsIdentity.FindFirst(type);
                if (claim != null) break;
            }

            return claim != null ? claim.Value : string.Empty;
        }

        public static string TenantId(this ClaimsIdentity claimsIdentity)
        {
            var id = claimsIdentity.SafeFindFirst(ClaimTypeTenantId);
            if (id == string.Empty)
            {
                var issuer = claimsIdentity.Issuer();
                id = issuer.RightOf("//").RightOf("/").LeftOf("/");
            }
            return id;
        }

        public static string TenantId(this ClaimsPrincipal claimsIdentity)
        {
            var id = claimsIdentity.SafeFindFirst(ClaimTypeTenantId);
            if (id == string.Empty)
            {
                var issuer = claimsIdentity.Issuer();
                id = issuer.RightOf("//").RightOf("/").LeftOf("/");
            }
            return id;
        }

        public static string Issuer(this ClaimsIdentity claimsIdentity)
        {
            try
            {
                return (claimsIdentity.FindFirst(ClaimTypes.Name) ?? claimsIdentity.FindFirst(ClaimTypes.NameIdentifier)).Issuer;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string Issuer(this ClaimsPrincipal claimsIdentity)
        {
            try
            {
                return claimsIdentity.Claims.Any()
                    ? claimsIdentity.Claims.First().Issuer
                    : string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string ObjectIdentifier(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypeObjectId);
        }

        public static string ObjectIdentifier(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypeObjectId);
        }

        public static string FullName(this ClaimsIdentity claimsIdentity)
        {
            var name = claimsIdentity.FirstName() + " " + claimsIdentity.LastName();
            return name.Trim() == "" ? claimsIdentity.SafeFindFirst(ClaimTypes.Name) : name;
        }

        public static string FullName(this ClaimsPrincipal claimsIdentity)
        {
            var name = claimsIdentity.FirstName() + " " + claimsIdentity.LastName();
            return name.Trim() == "" ? claimsIdentity.SafeFindFirst(ClaimTypes.Name) : name;
        }

        public static string FirstName(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypes.GivenName, ClaimTypeFirstName).Trim();
        }

        public static string FirstName(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypes.GivenName, ClaimTypeFirstName).Trim();
        }

        public static string LastName(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypes.Surname, ClaimTypeLastName).Trim();
        }

        public static string LastName(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypes.Surname, ClaimTypeLastName).Trim();
        }


        public static Claim Scope(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.FindFirst(ClaimTypeScope);
        }

        public static void AddScope(this ClaimsIdentity claimsIdentity, string scope)
        {
            claimsIdentity.AddClaim(new Claim(ClaimTypeScope, scope));
        }

        public static string HashKey(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.TenantId() + "|" + claimsIdentity.ObjectIdentifier();
        }

        public static string HashKey(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.TenantId() + "|" + claimsIdentity.ObjectIdentifier();
        }

        public static bool IsExternalUser(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.FindFirst(ClaimTypeB2cProvider) != null;
        }

        public static string Email(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypes.Email, ClaimTypeEmails);
        }

        public static string B2cFlow(this ClaimsIdentity claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypeB2cProvider);
        }
        public static string B2cFlow(this ClaimsPrincipal claimsIdentity)
        {
            return claimsIdentity.SafeFindFirst(ClaimTypeB2cProvider);
        }

        public static string IdentifierProvider(this ClaimsPrincipal claimsIdentity)
        {
            var provider = claimsIdentity.SafeFindFirst(ClaimTypeIdentityProvider);
            return provider;
        }


    }
}