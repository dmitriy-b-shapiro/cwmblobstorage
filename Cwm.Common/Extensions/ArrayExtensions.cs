﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Cwm.Common.Extensions
{
    /// <summary>
    /// Extension functions
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// Clears the array
        /// </summary>
        /// <param name="array"></param>
        public static void Clear<T>(this T[] array)
        {
            if (array == null) return;
            Array.Clear(array, 0, array.Length);
        }

        public static SecureString ToSecureString(this char[] value)
        {
            var secure = new SecureString();
            foreach (var c in value)
            {
                secure.AppendChar(c);
            }

            return secure;
        }        

    }
}