﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Cwm.Common.Extensions
{
    public static class StringExtensions
    {
        public static byte[] FromBase64(this string value)
        {
            if (value == null) throw new ArgumentNullException("value");
            return Convert.FromBase64String(value.Replace('-', '+').Replace('_', '/'));
        }

        public static string ToBase64(this byte[] inArray, bool urlSafe = false)
        {
            if (inArray == null) throw new ArgumentNullException("inArray");
            var value = Convert.ToBase64String(inArray);
            return urlSafe ? value.Replace('+', '-').Replace('/', '_') : value;
        }

        public static string ToBase64(this string s)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
        }

        public static string ConvertToUnsecureString(this SecureString secureString)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        public static string RightOf(this string s, string p, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(s) || string.IsNullOrEmpty(p)) return s;
            var n = s.IndexOf(p, GetComparisonType(ignoreCase));
            return n >= 0 ? s.Substring(n + p.Length) : String.Empty;
        }

        public static string LeftOf(this string s, string p, bool ignoreCase = false)
        {
            if (string.IsNullOrEmpty(s) || string.IsNullOrEmpty(p)) return s;
            var n = s.IndexOf(p, GetComparisonType(ignoreCase));
            return n >= 0 ? s.Substring(0, n) : s;
        }


        private static StringComparison GetComparisonType(bool ignoreCase)
        {
            return ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal;
        }

        public static string NeutralizeUserInput(this string input)
        {
            return input.Replace("\r", string.Empty)
                         .Replace("%0d", string.Empty)
                         .Replace("%0D", string.Empty)
                         .Replace("\n", string.Empty)
                         .Replace("%0a", string.Empty)
                         .Replace("%0A", string.Empty);
        }
    }
}