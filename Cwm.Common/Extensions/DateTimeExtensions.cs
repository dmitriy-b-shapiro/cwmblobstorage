﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cwm.Common.Extensions
{
    /// <summary>
    /// Extensions for Date/Time
    /// </summary>
    public static class DateTimeExtenstions
    {

        /// <summary>
        /// Calculates the Daylight savings time offset
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        public static int GetDstOffsetMinutes(this DateTime dateTime, TimeZoneInfo timeZone)
        {
            var offsetMinutes = 0;
            if (timeZone.SupportsDaylightSavingTime && timeZone.IsDaylightSavingTime(dateTime))
            {
                offsetMinutes = 60;
            }
            return offsetMinutes;
        }

        /// <summary>
        /// Converts the time into UTC given the appropriate time zone
        /// </summary>
        /// <param name="dateTime">Time to convert</param>
        /// <param name="timeZone">Timezone of datetime</param>
        /// <returns></returns>
        public static DateTime ToUniversalTime(this DateTime dateTime, TimeZoneInfo timeZone)
        {
            return dateTime - timeZone.GetUtcOffset(dateTime);
        }

        /// <summary>
        /// Converts the time into UTC given the appropriate time zone
        /// </summary>
        /// <param name="dateTime">Time to convert</param>
        /// <param name="timeZone">Timezone of datetime</param>
        /// <returns></returns>
        public static DateTime ToLocalTime(this DateTime dateTime, TimeZoneInfo timeZone)
        {
            return dateTime + timeZone.GetUtcOffset(dateTime);
        }

    }
}
