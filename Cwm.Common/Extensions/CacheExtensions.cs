﻿using System.Collections.Generic;
using System.Linq;
using Cwm.Common.Interfaces;
using Humanizer;

namespace Cwm.Middleware
{
    public static class CacheExtensions
    {
       
        public static string GetActivationCode(this IPersistedCache2020 cache, IUserSession userSession)
        {
            return cache.Get<string>(userSession,  userSession.UserId +"|activationCode");
        }
        public static void SetActivationCode(this IPersistedCache2020 cache, IUserSession userSession, string code, int cacheDurationInSeconds)
        {
            cache.AddOrUpdate(userSession, userSession.UserId + "|activationCode", code ?? "", cacheDurationInSeconds);
        }
    }
}