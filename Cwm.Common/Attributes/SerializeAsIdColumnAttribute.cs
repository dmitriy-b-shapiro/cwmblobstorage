﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cwm.Common.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SerializeAsIdLong : Attribute
    {
    }
    public class SerializeAsIdGuid : Attribute
    {
    }
    public class SerializeAsIdString : Attribute
    {
    }
}
