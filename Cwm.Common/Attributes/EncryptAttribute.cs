﻿using System;

namespace Cwm.DomainModel.Encryption
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class EncryptAttribute : Attribute
    {
        
    }
}
