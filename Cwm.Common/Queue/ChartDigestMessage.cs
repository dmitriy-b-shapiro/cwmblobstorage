﻿using System;

namespace Cwm.Common.Queue
{
    public class ChartDigestMessage : QueueMessageBase
    {
        public Guid DigestId { get; set; }
    }
}
