﻿using Cwm.Common.Interfaces;

namespace Cwm.Common.Caching
{
    public class MockPersistedCache2020 : InMemoryCache, IPersistedCache2020
    {
        public ITenantRepository TenantRepository { get; set; }
    }
}