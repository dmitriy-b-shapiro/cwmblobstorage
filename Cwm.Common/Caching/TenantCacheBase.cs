using Cwm.Common.Interfaces;
using Humanizer;

namespace Cwm.Common.Caching
{
    public abstract class TenantCacheBase
    {
        public string TenantKey(IUserSession userSession, string key)
        {
            return "{0}:{1}".FormatWith(userSession?.TenantId, key);
        }
    }
}