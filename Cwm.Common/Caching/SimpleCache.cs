﻿using System;
using LazyCache;

namespace Cwm.Common.Caching
{
    /// <summary>
    /// Provides a simple in-memory local cache. To use define it as a static field in an appropriate class.
    /// </summary>    
    public class SimpleCache<T> where T : class
    {
        private readonly int _expirationSeconds;
        private DateTime _cacheDirtyTime;
        private T _currentValue;
        private readonly object _syncLock = new object();

        public SimpleCache(int expirationSeconds = 60)
        {
            _expirationSeconds = expirationSeconds;
            ResetDirtyCacheTime();
        }

        private void ResetDirtyCacheTime()
        {
            _cacheDirtyTime = DateTime.UtcNow.AddSeconds(_expirationSeconds);
        }


        public T Get(Func<T> fetchValue)
        {
            if (_currentValue != null && DateTime.UtcNow <= _cacheDirtyTime) return _currentValue;
            lock (_syncLock)
            {
                if (_currentValue == null || DateTime.UtcNow > _cacheDirtyTime)
                {
                    _currentValue = fetchValue();
                }
            }
            ResetDirtyCacheTime();
            return _currentValue;
        }
    }
}