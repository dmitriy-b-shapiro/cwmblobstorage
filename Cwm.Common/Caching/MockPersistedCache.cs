using Cwm.Common.Interfaces;

namespace Cwm.Common.Caching
{
    public class MockPersistedCache : InMemoryCache, IPersistedCache { }
}