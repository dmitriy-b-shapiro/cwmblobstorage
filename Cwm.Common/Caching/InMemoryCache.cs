﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Cwm.Common.Interfaces;

namespace Cwm.Common.Caching
{
    public class InMemoryCache : TenantCacheBase, IMemoryCache
    {
        private readonly MemoryCache _cache;
        private readonly object _syncLock = new object();

        public InMemoryCache(string name)
        {
            _cache = new MemoryCache(name);
        }
        
        public InMemoryCache()
        {
            _cache = MemoryCache.Default;
        }

        public void AddOrUpdate(IUserSession userSession, string key, object value, int expiresInSeconds = 300, bool sliding = true)
        {
            var tkey = TenantKey(userSession, key);
            var policy = GetPolicy(expiresInSeconds, sliding);
            lock (_syncLock)
            {
                AddOrUpdate(tkey, value, policy);
            }
        }

        public void Remove(IUserSession userSession, string key)
        {
            var tkey = TenantKey(userSession, key);
            //If not there, OK. No need to lock.
            _cache.Remove(tkey);
        }

        public T Get<T>(IUserSession userSession, string key) where T : class
        {
            var tkey = TenantKey(userSession, key);
            //If not there, OK. No need to lock.
            var result = _cache.Get(tkey);
            return (T)result;
        }

        public T GetOrAdd<T>(IUserSession userSession, string key, Func<T> func, int expiresInSeconds = 300, bool sliding = true) where T : class
        {
            var tkey = TenantKey(userSession, key);
            var policy = GetPolicy(expiresInSeconds, sliding);
            //try get first without lock. 
            var data = _cache.Get(tkey);
            if (data != null) return data as T;
            //since it wasn't there, we lock. We test again before we add.
            lock (_syncLock)
            {
                if (!_cache.Contains(tkey))
                {
                    _cache.Add(new CacheItem(tkey, func()), policy);
                }
                return _cache.Get(tkey) as T;
            }
        }

        public void Clear(IUserSession userSession)
        {
            lock (_syncLock)
            {
                _cache.Where(c => c.Key.StartsWith(userSession.TenantId + ":" + userSession.UserId)).ToList().ForEach(c => _cache.Remove(c.Key));
                //_cache.Trim(100);
            }
        }

        private CacheItemPolicy GetPolicy(int expiresInSeconds, bool sliding)
        {
            return expiresInSeconds == 0
                ? new CacheItemPolicy()
                : sliding
                    ? new CacheItemPolicy { SlidingExpiration = TimeSpan.FromSeconds(expiresInSeconds) }
                    : new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddSeconds(expiresInSeconds) };
        }

        private void AddOrUpdate(string key, object value, CacheItemPolicy policy)
        {
            if (_cache.Contains(key)) _cache.Remove(key);
            _cache.Add(new CacheItem(key, value), policy);
        }
    }

    public class InMemoryCache2 : TenantCacheBase, IMemoryCache
    {
        private readonly MemoryCache _cache;
        private readonly ConcurrentDictionary<string, object> _sessionLocks = new ConcurrentDictionary<string, object>();

        public InMemoryCache2(string name)
        {
            _cache = new MemoryCache(name);
        }

        public InMemoryCache2()
        {
            _cache = MemoryCache.Default;
        }

        private object GetSessionLock(string key)
        {
            var sessionLock = _sessionLocks.GetOrAdd(key, (tempKey) =>   new object());

            return sessionLock;
        }

        public void AddOrUpdate(IUserSession userSession, string key, object value, int expiresInSeconds = 300, bool sliding = true)
        {
            var tkey = TenantKey(userSession, key);
            var policy = GetPolicy(expiresInSeconds, sliding);
            lock (GetSessionLock(tkey))
            {
                AddOrUpdate(tkey, value, policy);
            }
        }

        public void Remove(IUserSession userSession, string key)
        {
            var tkey = TenantKey(userSession, key);
            _sessionLocks.TryRemove(tkey, out var bogusValue);
            //If not there, OK. No need to lock.
            _cache.Remove(tkey);
        }

        public T Get<T>(IUserSession userSession, string key) where T : class
        {
            var tkey = TenantKey(userSession, key);
            //If not there, OK. No need to lock.
            var result = _cache.Get(tkey);
            return (T)result;
        }

        public T GetOrAdd<T>(IUserSession userSession, string key, Func<T> func, int expiresInSeconds = 300, bool sliding = true) where T : class
        {
            var tkey = TenantKey(userSession, key);
            var policy = GetPolicy(expiresInSeconds, sliding);
            //try get first without lock. 
            var data = _cache.Get(tkey);
            if (data != null) return data as T;
            //since it wasn't there, we lock. We test again before we add.
            lock (GetSessionLock(tkey))
            {
                if (!_cache.Contains(tkey))
                {
                    _cache.Add(new CacheItem(tkey, func()), policy);
                }
                return _cache.Get(tkey) as T;
            }
        }

        //public void Clear(IUserSession userSession)
        //{
        //    lock (_syncLock)
        //    {
        //        _cache.Where(c => c.Key.StartsWith(userSession.TenantId + ":" + userSession.UserId)).ToList().ForEach(c => _cache.Remove(c.Key));
        //        //_cache.Trim(100);
        //    }
        //}

        private CacheItemPolicy GetPolicy(int expiresInSeconds, bool sliding)
        {
            return expiresInSeconds == 0
                ? new CacheItemPolicy()
                : sliding
                    ? new CacheItemPolicy { SlidingExpiration = TimeSpan.FromSeconds(expiresInSeconds) }
                    : new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddSeconds(expiresInSeconds) };
        }

        private void AddOrUpdate(string key, object value, CacheItemPolicy policy)
        {
            _cache.Remove(key);
            _cache.Add(new CacheItem(key, value), policy);
        }
    }

}