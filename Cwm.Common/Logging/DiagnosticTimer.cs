﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Cwm.Common.Logging
{
    public class DiagnosticTimer
    {
        private readonly Stopwatch _stopwatch;
        private readonly Dictionary<int, long> _times= new Dictionary<int, long>();

        public DiagnosticTimer()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public void Resume()
        {
            _stopwatch.Start();
        }

        public void Pause()
        {
            _stopwatch.Stop();
        }
        public void Stop()
        {
            _stopwatch.Stop();
        }

        public void Mark(int id)
        {
            _times[id] = _stopwatch.ElapsedMilliseconds;
        }
        public void MarkAndStop(int id)
        {
            _times[id] = _stopwatch.ElapsedMilliseconds;
            _stopwatch.Stop();
        }

       /// <summary>
        /// Gets the marked elapsed time. If previousId  != null, the difference is returned
        /// </summary>
        /// <param name="id"></param>
        /// <param name="previousId"></param>
        /// <returns></returns>
        public long GetElapsed(int id, int? previousId = null)
        {
            return previousId != null ? _times[id] - _times[previousId.Value] : _times[id];

        }

    }
}