using System;
using Cwm.Common.Interfaces;

namespace Cwm.Common.Logging
{
    public sealed class LogManager
    {
        public static ILog GetLogger(Type type)
        {
            return new LogImpl(type);
        }
    }
}