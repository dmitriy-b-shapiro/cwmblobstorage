using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using Cwm.Common.Extensions;
using Cwm.Common.Interfaces;
using Humanizer;

namespace Cwm.Common.Logging
{
    internal class LogImpl : ILog
    {
        private readonly string _typeName;

        public LogImpl(Type type)
        {
            _typeName = type.FullName;
        }

        public void Info(string message, Exception exception = null, [CallerMemberName] string caller = null)
        {
            Trace.TraceInformation("INFO|{0}|{4}|{1}|{2}|{3}"
                .FormatWith(DateTime.UtcNow, GetContextString(caller), message, ExceptionUtils.FormatException(exception, true, true), GetCurrentTenant()));
        }

        public void Report(string message, Exception exception = null, [CallerMemberName] string caller = null)
        {
            Trace.TraceWarning("REPORT|{0}|{4}|{1}|{2}|{3}"
                .FormatWith(DateTime.UtcNow, GetContextString(caller), message, ExceptionUtils.FormatException(exception, true, true), GetCurrentTenant()));
        }
        public void Warn(string message, Exception exception = null, [CallerMemberName] string caller = null)
        {
            Trace.TraceWarning("WARN|{0}|{4}|{1}|{2}|{3}".FormatWith(DateTime.UtcNow, GetContextString(caller), message, ExceptionUtils.FormatException(exception, true, true), GetCurrentTenant()));
        }

        public void Error(string message, Exception exception = null, [CallerMemberName] string caller = null)
        {
            Trace.TraceError("ERROR|{0}|{4}|{1}|{2}|{3}".FormatWith(DateTime.UtcNow, GetContextString(caller), message, ExceptionUtils.FormatException(exception, true, true), GetCurrentTenant()));
        }

        private string GetContextString(string caller)
        {
            var contextString = string.Format("{0}|{1}", _typeName, caller ?? "");
            return contextString;
        }

        private string GetCurrentTenant()
        {
            try
            {
                var issuer = (ClaimsPrincipal.Current?.Identity?.Name ?? string.Empty).RightOf("@");
                return issuer;
            }
            catch (Exception ex)
            {
                //eat it for now...
            }
            return string.Empty;
        }
    }
}