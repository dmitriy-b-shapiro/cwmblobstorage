using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace Cwm.Common
{
    public sealed class Utilities
    {
        public static void SerializeToXmlFile<T>(T @object, string xmlFile)
        {
            using (var sw = new StreamWriter(xmlFile))
            {
                var ser = new XmlSerializer(typeof(T));
                ser.Serialize(sw, @object);
            }
        }

        public static T DeserializeFromXmlFile<T>(string xmlFile)
        {
            using (var sr = new StreamReader(xmlFile))
            {
                return DeserializeFromStream<T>(sr);
            }
        }

        public static T DeserializeFromXmlString<T>(string xmlFileContents)
        {
            using (var sr = new StringReader(xmlFileContents))
            {
                return DeserializeFromStream<T>(sr);
            }
        }

        public static T DeserializeFromStream<T>(TextReader stream)
        {
            var ser = new XmlSerializer(typeof(T));
            return (T)ser.Deserialize(stream);
        }

        public static string Extract(string resource, Assembly useAssembly = null)
        {
            var assembly = useAssembly ?? Assembly.GetCallingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resource))
            using (var reader = new StreamReader(stream))
            {
                var content = reader.ReadToEnd();
                return content;
            }
        }

        /// <summary>
        /// Escapes HTTP Header values - copied from internal function System.Web.Util.HttpEncoder.HeaderEncodeInternal
        /// .Net Framework 4.8 (https://referencesource.microsoft.com/#System.Web/Util/HttpEncoder.cs,56478936b23b094c)
        /// On 4/29/2019
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string EscapeHttpHeaderValue(string value)
        {
            string sanitizedHeader = value;
            if (HeaderValueNeedsEncoding(value))
            {
                // DevDiv Bugs 146028
                // Denial Of Service scenarios involving 
                // control characters are possible.
                // We are encoding the following characters:
                // - All CTL characters except HT (horizontal tab)
                // - DEL character (\x7f)
                var sb = new StringBuilder();
                foreach (char c in value)
                {
                    if (c < 32 && c != 9)
                    {
                        sb.Append(_headerEncodingTable[c]);
                    }
                    else if (c == 127)
                    {
                        sb.Append("%7f");
                    }
                    else
                    {
                        sb.Append(c);
                    }
                }
                sanitizedHeader = sb.ToString();
            }

            return sanitizedHeader;
        }

        private static readonly string[] _headerEncodingTable = new string[] {
            "%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07",
            "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f",
            "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
            "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f"
        };

        // Returns true if the string contains a control character (other than horizontal tab) or the DEL character.
        private static bool HeaderValueNeedsEncoding(string value)
        {
            foreach (char c in value)
            {
                if ((c < 32 && c != 9) || (c == 127))
                {
                    return true;
                }
            }
            return false;
        }


    }
}