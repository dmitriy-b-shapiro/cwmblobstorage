﻿using System;
using System.Threading;

namespace Cwm.Common
{
    public static class RetryLogic
    {
        private static readonly Random Random;
        private static readonly object RandomSyncLock;

        // static ctor guaranteed by .NET to be called by only one thread
        static RetryLogic()
        {
            Random = new Random();
            RandomSyncLock = new object();
        }

        public static void RetryOnException<TException>(Action callback, int retryCount = 6, Action<TException> exceptionCallback = null) where TException : Exception
        {
            var retries = 0;
            var retry = false;
            do
            {
                try
                {
                    callback();
                    return;
                }
                catch (TException ex)
                {                    
                    if (exceptionCallback != null) exceptionCallback(ex);

                    retry = (retries++) < retryCount;
                    if (!retry) continue;

                    int wait;
                    lock (RandomSyncLock) // need a critical section around this because the Next instance method isn't thread safe
                    {
                        wait = Random.Next(1, 100);
                    }
                    Thread.Sleep(wait); // Wait between [1,100] ms before retry
                }
            }
            while (retry);
        }
    }
}
