﻿using System.Configuration;

namespace Cwm.Common.Config
{
    public static class CwmConstants
    {
        private static string _version;

        public static string IssuerTemplate => "https://sts.windows.net/{0}/";
        public static string CareCoordinatorRole => "Care Coordinator";
        public static string LimitedCareCoordinatorRole => "Limited Care Coordinator";
        public static string LocalAdminRole => "Local Admin";
        public static string AdminRole => "Admin";

        public static string ExternalUserRole => "ExternalUser";
        public static string ClientApplicationRole => "Client Credentials";
        public static string DataSourceSpecificAccessRole => "DataSourceSpecific";
        public static string ProgramSpecificAccessRole => "ProgramSpecific";
        public static string GroupSpecificAccessRole => "GroupSpecific";
        public static string CurrentVersion => GetVersion();
        public static string DataSourceIdsRoleName = "http://schemas.enli.net/ws/2016/11/identity/claims/datasourceids";

        private static string GetVersion()
        {
            if (!string.IsNullOrWhiteSpace(_version)) return _version;
            _version = ConfigurationManager.AppSettings["cwm.Release.Number"];
            return _version;
        }
    }
}