﻿using System;
using System.Configuration;
using Cwm.Common.Interfaces;
using Cwm.Common.Logging;
using Humanizer;

namespace Cwm.Common.Config
{
    public class CwmAppConfig : ICwmConfig
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CwmAppConfig));

        public ConnectionStringSettingsCollection ConnectionStrings { get { return ConfigurationManager.ConnectionStrings; } }

        public string CorsOrigins { get { return GetAppSetting("cwm.CorsOrigins"); } }
        public string CacheConnectionString { get { return GetConnectionString("cwm.Cache"); } }
        public string SignalRCacheConnectionString { get { return GetConnectionString("cwm.SignalRCache"); } }
        public string SignalRServiceBusCacheConnectionString { get { return GetConnectionString("cwm.SignalRServiceBusCache"); } }
        public string ApplicationTenant { get { return GetAppSetting("aad.ApplicationTenant"); } }
        public string CwmApiResourceId { get { return GetAppSetting("aad.ApiResourceId"); } }
        public string ClientId { get { return GetAppSetting("aad.ClientId"); } }
        public string ClientKey { get { return GetAppSetting("aad.ClientKey"); } }
        public string StorageConnectionString { get { return GetConnectionString("cwm.Storage"); } }
        public string ReleaseNumber { get { return GetAppSetting("cwm.Release.Number"); } }
        public string Environment { get { return GetAppSetting("cwm.Environment"); } }
        public string CwmServiceUrl { get { return GetAppSetting("cwm.ServiceUrl"); } }
        public string KeyVaultName { get { return GetAppSetting("azure.KeyVaultName"); } }

        public bool IsRunningLocally { get { return Environment.Equals("cwmlocal", StringComparison.OrdinalIgnoreCase); } }
        public bool IsProduction { get { return Environment.StartsWith("StratusProd", StringComparison.OrdinalIgnoreCase); } }

        public string GraphApiResourceId { get { return GetAppSetting("aad.GraphApiResourceId"); } }
        public string CommonAuthority { get { return GetAppSetting("aad.CommonAuthority"); } }
        public string OAuthBadRequestMsg { get { return GetAppSetting("aad.oauth.BadRequestMsg"); } }
        public string OAuthSuccessMsg { get { return GetAppSetting("aad.oauth.SuccessMsg"); } }
        public string AppErrorMessage { get { return GetAppSetting("cwm.AppErrorMessage"); } }
        public string InvalidTenantMessage { get { return GetAppSetting("aad.InvalidTenantMessage"); } }
        public string GenericErrorMessage { get { return GetAppSetting("aad.GenericErrorMessage"); } }
        public string UserNotActivatedMessage { get { return GetAppSetting("aad.UserNotActivatedMessage"); } }

        public AllowedAuthenticationTypes AllowedAuthSchemas
        {
            get
            {
                switch (GetAppSetting("AllowedAuthSchemas"))
                {
                    case "All":
                        return AllowedAuthenticationTypes.All;
                    case "B2C":
                        return AllowedAuthenticationTypes.B2C;
                    default:
                        return AllowedAuthenticationTypes.Classic;                    
                }
            }
        }

        public string TenantAuthority(string tenantId)
        {
            return "https://login.windows.net/{0}".FormatWith(tenantId);
        }

        public int MinutesToCacheExternalUserCredentials =>
            Convert.ToInt32(GetAppSetting("cwm.MinutesToCacheExtUserCredentials"));

        public string AthenaApiUrl => GetAppSetting("athena.ApiUrl");
        public string AthenaAuthUrl => GetAppSetting("athena.AuthUrl");
        public string AthenaAuthClientId => GetAppSetting("athena.ClientId");
        public string AthenaAuthClientKey => GetAppSetting("athena.ClientKey");
        public string AthenaAuthScope => GetAppSetting("athena.Scope");

        public string KmsKeyId => GetAppSetting("aws.kmsKeyId");

        public int AthenaApiTimeoutInSeconds => Convert.ToInt32(GetAppSetting("athena.TimeoutInSeconds"));

        public bool UseRedisForAthena => GetAppSetting("athena.UseRedisCache") == bool.TrueString;
        public bool ExportReportsToBlob => GetAppSetting("cwm.ExportReportsToBlob") == bool.TrueString;
        public int ExportedReportRetentionInDays => Convert.ToInt32(GetAppSetting("cwm.ExportReportsRetentionInDays"));

        public int ExportReportsProgressIntervalInSeconds => Convert.ToInt32(GetAppSetting("cwm.ExportReportsProgressIntervalInSeconds"));

        public bool VerboseDebugLogging => GetAppSetting("cwm.VerboseDebugLogging") == bool.TrueString;

        private static string GetConnectionString(string key)
        {
            try
            {
                var s = ConfigurationManager.ConnectionStrings[key].ConnectionString;
                return s;
            }
            catch (Exception ex)
            {
                Log.Error("Application tried to get config value for nonexistant key: " + key, ex);
                throw;
            }
        }

        private static string GetAppSetting(string key)
        {
            try
            {
                var s = ConfigurationManager.AppSettings[key];
                return s;
            }
            catch (Exception ex)
            {
                Log.Error("Application tried to get config value for nonexistant key: " + key, ex);
                throw;
            }
        }
    }
}