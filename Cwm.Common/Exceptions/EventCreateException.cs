﻿

using System;

namespace Cwm.Common.Exceptions
{
    public class EventCreateException : BadRequestException
    {
        public string VisibleId { get; private set; }
        public DateTime EventStart { get; private set; }
        public long EventTypeId { get; private set; }
        public bool Duplicate { get; private set; }

        public EventCreateException(string message, string visibleId, DateTime eventStart, long eventTypeId, bool duplicate = false) 
            : base(message)
        {
            VisibleId = visibleId;
            EventStart = eventStart;
            EventTypeId = eventTypeId;
            Duplicate = duplicate;
        }
    }
}
