﻿using System;
using System.Linq;

namespace Cwm.Common.Exceptions
{
    public class DuplicateIndexException : Exception
    {
        public string IndexName { get; private set; }
        public object Entity { get; private set; }
        public string ErrorMessage { get; private set; }
        public DuplicateIndexException(string message, object entity)
        {
            IndexName = message.Split('\'').FirstOrDefault(s => s.StartsWith("IX_"));
            Entity = entity;
            ErrorMessage = IndexName != null
                ? $"Could not update database due to duplicate key entry in index {IndexName}"
                : "Could not update database due to duplicate key entry in unknown index";
        }
    }
}
