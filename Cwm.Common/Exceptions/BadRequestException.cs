﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Humanizer;

namespace Cwm.Common.Exceptions
{
    public enum BadRequestType
    {
        [Description("invalid_value")]
        InvalidValue,
        [Description("missing_required_field")]
        MissingRequiredValue,
        [Description("referenced_item_not_found")]
        ReferencedItemNotFound,
        [Description("duplicate_value_found")]
        ConflictingValue,
        [Description("referenced_item_found")]
        ReferenceConstraint
    }

    public class BadRequestMessage
    {
        public BadRequestMessage()
        {
            Errors = new List<BadRequestItem>();
        }

        public BadRequestMessage(string summary,string property, string message, BadRequestType badRequestType = BadRequestType.InvalidValue)
        {
            Summary = summary;
            Errors = new List<BadRequestItem>
            {
                new BadRequestItem(property, message, badRequestType)
            };
        }

        public string Summary { get; set; }
        public List<BadRequestItem> Errors { get; set; }
    }

    public class BadRequestItem
    {
        public BadRequestItem(string propertyName, string message, BadRequestType badRequestType)
        {
            PropertyName = propertyName;
            Message = message;
            ErrorCode = badRequestType.Humanize();
        }

        public string PropertyName { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
    }

    public class BadRequestException : Exception
    {
        public BadRequestMessage BadRequestMessage { get; private set; }

        public BadRequestException(string msg) : base(msg) { }

        public BadRequestException(BadRequestMessage msg) : base(msg.Summary)
        {
            BadRequestMessage = msg;
        }
    }
}
