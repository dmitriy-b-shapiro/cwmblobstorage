using System;
using System.IO;
using System.Threading.Tasks;

namespace Cwm.Common.Interfaces
{
    public interface IBlobRepository
    {
        Task GetDocumentAsync(IUserSession userSession, Guid id, Stream stream);
        Task SaveDocumentAsync(IUserSession userSession, Guid id, Stream stream);
        Task DeleteDocumentAsync(IUserSession userSession, Guid id);
        Task<string> GetDocumentUnencryptedAsync(IUserSession userSession, Guid id);
        Task SaveDataUnencryptedAsync(IUserSession userSession, string uniqueId, string data);
        Task<(byte[] data, string filename)> GetFileAsync(IUserSession userSession, Guid id);
        Task SaveFileAsync(IUserSession userSession, string uniqueId, string filename, byte[] data);
    }
}