﻿using System.Configuration;

namespace Cwm.Common.Interfaces
{
    public enum AllowedAuthenticationTypes
    {
        Classic,
        All,
        B2C,
    }
    public interface ICwmConfig
    {
        ConnectionStringSettingsCollection ConnectionStrings { get; } 

        string CorsOrigins { get; }
        string CacheConnectionString { get; }
        string SignalRCacheConnectionString { get; }
        string ApplicationTenant { get; }
        string CwmApiResourceId { get; }
        string ClientId { get; }
        string ClientKey { get; }
        string StorageConnectionString { get; }
        string ReleaseNumber { get; }
        string Environment { get; }
        string CwmServiceUrl { get; }
        string KeyVaultName { get; }

        bool IsRunningLocally { get; }
        bool IsProduction { get; }

        string GraphApiResourceId { get; }
        string CommonAuthority { get; }
        string OAuthBadRequestMsg { get; }
        string OAuthSuccessMsg { get; }
        string AppErrorMessage { get; }
        string InvalidTenantMessage { get; }
        string GenericErrorMessage { get; }
        string UserNotActivatedMessage { get; }
        string TenantAuthority(string tenantId);

        bool VerboseDebugLogging { get; }

        AllowedAuthenticationTypes AllowedAuthSchemas { get; }
        int MinutesToCacheExternalUserCredentials { get; }

        string AthenaApiUrl { get; }
        string AthenaAuthUrl { get; }
        string AthenaAuthClientId { get; }
        string AthenaAuthClientKey { get; }

        string AthenaAuthScope { get; }

        string KmsKeyId { get; }

        int AthenaApiTimeoutInSeconds { get; }
        bool UseRedisForAthena { get; }

        bool ExportReportsToBlob { get; }
        int ExportedReportRetentionInDays { get; }

        int ExportReportsProgressIntervalInSeconds { get; }
    }
}