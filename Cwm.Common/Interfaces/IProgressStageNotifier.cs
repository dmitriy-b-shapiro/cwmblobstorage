﻿namespace Cwm.Common.Interfaces
{
    public interface IProgressStageNotifier
    {
        void SetStage(int id, string name);
    }
}