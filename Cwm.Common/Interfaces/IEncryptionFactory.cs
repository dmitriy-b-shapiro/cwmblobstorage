﻿namespace Cwm.Common.Interfaces
{
    public interface IEncryptionFactory
    {
        IEncryption GetEncryption(IUserSession userSession);
    }
}