using System;
using System.Threading.Tasks;
using Cwm.Common.Models;

namespace Cwm.Common.Interfaces
{
    public interface ITokenService
    {
        AuthToken GetUserLevelTokenFromAccessCode(IUserSession userSession, string code, string resourceId, Uri sourceUri);
        AuthToken GetExternalUserLevelTokenFromAccessCode(IUserSession userSession, string code, string resourceId, Uri sourceUri, string activationCode);
        AuthToken GetApplicationLevelToken(IUserSession userSession, string resourceId, string tenantId);
        AuthToken GetUserLevelToken(IUserSession userSession, string resourceId, Uri sourceUri);
        AuthToken GetExternalUserLevelToken(IUserSession userSession, string resourceId, Uri sourceUri);
        void ClearUserToken(IUserSession userSession, string resourceId);
    }
}