﻿using System.Security;

namespace Cwm.Common.Interfaces
{
    public interface IEncryption
    {
        SecureString DatabaseConnectionString { get; }
        T EncryptEntity<T>(T entity) where T : class;
        bool DecryptEntity<T>(T entity) where T : class;
        string EncryptString(string str);
        string DecryptString(string str);

        byte[] Decrypt(byte[] str);
        byte[] Encrypt(byte[] str);
    }
}