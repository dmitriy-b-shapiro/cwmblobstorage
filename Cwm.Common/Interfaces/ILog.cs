﻿using System;
using System.Runtime.CompilerServices;

namespace Cwm.Common.Interfaces
{
    /// <summary>
    /// Logging interface. Note there are no Debug* methods because under the hood we are using System.Diagnostics.Trace API and specifically
    /// the TraceInformation, TraceWarning, and TraceError. This ties in with the setting up of Diagnostics for an Azure web role or website
    /// where we can set the logging level for the information captured to be informational, warning or error.
    /// See the following link for enabling/configuring diagnostics for an Azure Web Site in the Azure Management Portal:
    /// http://azure.microsoft.com/en-us/documentation/articles/web-sites-enable-diagnostic-log/
    /// See the following link for setting up diagnostics for an Azure role such as a web role in Visual Studio:
    /// http://msdn.microsoft.com/en-us/library/dn186185.aspx    
    /// </summary>
    public interface ILog
    {
        void Info(string message, Exception exception = null, [CallerMemberName] string caller = null);
        void Report(string message, Exception exception = null, [CallerMemberName] string caller = null);
        void Warn(string message, Exception exception = null, [CallerMemberName] string caller = null);
        void Error(string message, Exception exception = null, [CallerMemberName] string caller = null);
    }
}
