using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cwm.Common.Interfaces
{
    public interface IKeyVaultClient
    {
        Task<List<string>> GetSecretNames(IUserSession userSession);
        Task DeleteSecret(IUserSession userSession, string key);
        Task<string> CreateSecret(IUserSession userSession, string key, object value);
        Task<T> GetSecret<T>(IUserSession userSession, string key) where T : class;
        T GetSecretSync<T>(IUserSession userSession, string key) where T : class;
    }
}