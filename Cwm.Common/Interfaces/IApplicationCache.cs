﻿using System;

namespace Cwm.Common.Interfaces
{
    public interface IApplicationCache
    {
        void AddOrUpdate(IUserSession userSession, string key, object value, int expiresInSeconds = 300, bool sliding = true);
        void Remove(IUserSession userSession, string key);
        T Get<T>(IUserSession userSession, string key) where T : class;
        T GetOrAdd<T>(IUserSession userSession, string key, Func<T> func, int expiresInSeconds = 300, bool sliding = true) where T : class;
        //void Clear(IUserSession userSession);
    }
}
