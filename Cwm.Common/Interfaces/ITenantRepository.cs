﻿using System.Collections.Generic;
using Cwm.Common.Models;

namespace Cwm.Common.Interfaces
{
    public interface ITenantRepository
    {
        List<Tenant> GetTenants();
        Tenant Read(string tenantId);
        void Add(Tenant tenant);
        void Remove(Tenant tenant);
        void Update(Tenant tenant);

    }
}