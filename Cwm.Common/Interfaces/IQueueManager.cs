﻿using Cwm.Common.Queue;

namespace Cwm.Common.Interfaces
{
    public interface IQueueManager
    {
        void AddToQueue<T>(string queueName, T item) where T : QueueMessageBase;
    }
}