﻿namespace Cwm.Common.Interfaces
{
    public interface IPersistedCache : IApplicationCache { }

    public interface IPersistedCache2020 : IApplicationCache
    {
        ITenantRepository TenantRepository { set; }
        //Used to clear out all user session information
        void Clear(IUserSession session);
    }
}

