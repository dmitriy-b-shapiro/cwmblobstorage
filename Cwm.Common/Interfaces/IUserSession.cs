using System.Collections.Generic;

namespace Cwm.Common.Interfaces
{
    public interface IUserSession
    {
        string UserId { get; }
        string UserPrincipalName { get; }
        string TenantId { get; }
        string UserHostAddress { get; }
        bool IsAdmin { get; }
        bool IsLocalAdmin { get; }
        bool IsExternalUser { get; }
        List<string> Roles { get; } 
        string HashKey { get; }
        bool PersistingToken { get; set; }
        List<long> HasAccessToDataSourceIds { get; }
        string B2CFlowName { get; }
    }
}