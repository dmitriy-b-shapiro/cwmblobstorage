﻿namespace Cwm.Common.Interfaces
{
    public interface IProgressNotifier
    {
        void RecordsProcessed(int recordCount);
        void Message(string message, bool error = true);
        void Done(bool succeeded, int? recordsUpdated = null);
        void SetRecsTotal(int recsTotal);

        void ResetCount();
        void SetValues(int total, int recordCount);

        int CurrentTotal { get; }
        int CurrentProcessed { get; }

        void SignalKeepAlive();
    }
}
