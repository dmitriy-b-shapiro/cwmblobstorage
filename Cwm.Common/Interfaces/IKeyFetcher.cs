﻿using System.Security.Cryptography;

namespace Cwm.Common.Interfaces
{
    public interface IKeyFetcher
    {
        Aes GetKey(IUserSession userSession, Models.Tenant tenant);
        Aes GetKey(IUserSession userSession);
    }
}