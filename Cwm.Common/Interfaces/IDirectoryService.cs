﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Threading.Tasks;
using Cwm.Common.Models;

namespace Cwm.Common.Interfaces
{
    public interface IDirectoryService
    {
        Task<bool> AddApplicationToAadRole(string appId, string roleName,  Uri uri);
        Task<UserProfilePagedResult> FindUserProfiles(List<string> hiddenUsers, int count = 10, int page = 1, string search = null);
        Task<UserProfile> GetUserProfile(string userId = null);
        Task<UserProfile> AddUserProfile(AddUserProfile userProfile, SecureString pwd);
        Task<List<string>> FindGlobalAdmins();
        Task DeleteUserProfile(string userId);
        Task UpdateUserProfile(UpdateUserProfile userProfile);
        Task ResetUserPassword(string userId, SecureString newPassword, bool forceChangePasswordNextLogin);
        Task<TenantDetail> GetTenantDetail();

    }
}