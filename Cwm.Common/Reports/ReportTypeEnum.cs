﻿namespace Cwm.Common.Reports
{
    public enum ReportTypeEnum
    {
        ChartDigestHL7,
        ProgramActivityReport,
        ProgramStaffLoadReport,
        ChartDigest,
        WorkflowExport,
        CCMBillingReport,
        EventActivityReport, //added JC - Ryan created this report at the same time; this name might be  different in his code
        ProgramsList,//added JC 10/8/16
        TextMessagingUsageReport,
        PreVisitPrepReport,
        CareMessageExport,
        EventTaskCompletionReport,
        TcmCompletionReport,
        ReferralCompletionReport,
    }
}