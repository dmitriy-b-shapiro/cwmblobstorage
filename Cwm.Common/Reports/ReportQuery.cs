﻿using System;
using Cwm.Common.Models;

namespace Cwm.ModelsV2.Reports
{
    public class ReportQuery
    {
        public int ReportType { get; set; }
        public DateTime? ReportStartDate { get; set; }
        public DateTime? ReportEndDate { get; set; }
        public string ReportEndDateString { get; set; }
        public long? DataSourceId { get; set; }
        public PatientProgramStateEnum? ProgramState { get; set; }
        public bool? PageBreak { get; set; }
        public bool? ConsolidateReport { get; set; }
        public int? TimeZoneOffset { get; set; }
        public string ReportStartDateString { get; set; }
        public bool showPatientDetails { get; set; }
        public int DaysInAdvance { get; set; }
        public string Format { get; set; }

        public string Inclusion { get; set; }
        public string UserId { get; set; }
    }
}