using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Cwm.Common.Formatters
{
    /// <summary>
    /// Format phone numbers (borrowed from SMP)
    /// </summary>
    public class PhoneParser
    {
        private readonly List<string> _extensionCharacters = new List<string> { "x", "ext" };
        private string _numberWithoutFormatting;
        private string _originalNumberWithoutWhitespace;

        public PhoneParser()
        {
        }

        public PhoneParser(string phoneNumber)
        {
            OriginalNumber = phoneNumber;
        }

        private string OriginalNumber { get; set; }

        private string OriginalNumberWithoutWhitespace
        {
            get
            {
                if (String.IsNullOrEmpty(_originalNumberWithoutWhitespace))
                {
                    _originalNumberWithoutWhitespace = OriginalNumber == null ? "" : OriginalNumber.Trim().Replace(" ", "");
                }
                //return _originalNumberWithoutWhitespace;
                return ConfigurationManager.AppSettings["cwm.DebuggingEncryptedDatabase"] == bool.FalseString ? _originalNumberWithoutWhitespace : "";
            }
        }

        private string AreaCode
        {
            get
            {
                return HasAreaCode() ? NumberWithoutFormatting.Substring(0, 3) : "";
            }
        }

        private string Prefix
        {
            get
            {
                if (HasAreaCode() && NumberWithoutFormatting.Length >= 6)
                {
                    return NumberWithoutFormatting.Substring(3, 3);
                }
                return NumberWithoutFormatting.Length >= 3 ? NumberWithoutFormatting.Substring(0, 3) : "";
            }
        }

        private string Suffix
        {
            get
            {
                if (HasAreaCode() && NumberWithoutFormatting.Length >= 10 &&
                    (ExtenstionCharacterLocation == -1 || ExtenstionCharacterLocation > 10))
                {
                    return NumberWithoutFormatting.Substring(6, 4);
                }
                if (NumberWithoutFormatting.Length >= 7 &&
                    (ExtenstionCharacterLocation == -1 || ExtenstionCharacterLocation > 7))
                {
                    return NumberWithoutFormatting.Substring(3, 4);
                }
                return "";
            }
        }

        private int ExtenstionCharacterLocation
        {
            get
            {
                foreach (var ext in _extensionCharacters.Where(ext => OriginalNumberWithoutWhitespace.Contains(ext)))
                {
                    return OriginalNumberWithoutWhitespace.IndexOf(ext, StringComparison.Ordinal);
                }
                return -1;
            }
        }

        private string Extension
        {
            get
            {
                if (HasAreaCode() && HasExtension())
                {
                    return NumberWithoutFormatting.Substring(10);
                }
                if (!HasAreaCode() && HasExtension())
                {
                    return NumberWithoutFormatting.Substring(7);
                }

                return "";
            }
        }

        private string FormattedNumber
        {
            get
            {
                const string format = "{0} {1}-{2} {3}";

                if (!String.IsNullOrWhiteSpace(OriginalNumber) && OriginalNumber.Substring(0, 1) == "+")
                {
                    //international
                    return OriginalNumber;
                }

                if (!String.IsNullOrWhiteSpace(OriginalNumber) && OriginalNumber.Substring(0, 1) == "=")
                {
                    //international - athena workaround
                    //replace = with a +
                    return "+" + OriginalNumber.Substring(1, OriginalNumber.Length-1);
                }

                if (Prefix == "" || Prefix.Length < 3 ||
                    Suffix == "" || Suffix.Length < 4)
                {
                    return OriginalNumber;
                }

                return String.Format(format,
                    String.IsNullOrEmpty(AreaCode) ? "" : String.Format("({0})", AreaCode),
                    Prefix,
                    Suffix,
                    String.IsNullOrEmpty(Extension) ? "" : String.Format("x{0}", Extension)).Trim();
            }
        }

        private string NumberWithoutFormatting
        {
            get
            {
                if (!String.IsNullOrEmpty(_numberWithoutFormatting)) return _numberWithoutFormatting;

                var sb = new StringBuilder();

                foreach (var character in OriginalNumberWithoutWhitespace.Where(character => character >= char.Parse("0") && character <= char.Parse("9")))
                {
                    sb.Append(character);
                }

                _numberWithoutFormatting = sb.ToString();
                return _numberWithoutFormatting;
            }
        }

        private bool HasAreaCode()
        {
            if ((!HasExtension() && NumberWithoutFormatting.Length == 10) ||
                NumberWithoutFormatting.Length > 10 ||
                (HasExtension() && ExtenstionCharacterLocation != -1 && ExtenstionCharacterLocation > 10))
            {
                return true;
            }
            return _extensionCharacters.Select(ext => OriginalNumberWithoutWhitespace.IndexOf(ext, StringComparison.Ordinal)).Any(index => index >= 10);
        }

        private bool HasExtension()
        {
            if (NumberWithoutFormatting.Length > 10 ||
                _extensionCharacters.Exists(ext => OriginalNumberWithoutWhitespace.Contains(ext)))
            {
                return true;
            }
            return _extensionCharacters.Any(ext => OriginalNumberWithoutWhitespace.Contains(ext));
        }
        
        public override string ToString()
        {
            return FormattedNumber;
        }
    }
}