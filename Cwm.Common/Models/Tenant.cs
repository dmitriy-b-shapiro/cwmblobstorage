﻿using System;
using System.Runtime.Serialization;
using System.Text;
using Cwm.Common.Extensions;
using Humanizer;

namespace Cwm.Common.Models
{
    public enum TenantAuthenticationType
    {
        [EnumMember]
        Default,
        [EnumMember]
        B2C,
    }

    public class Tenant
    {
        public Tenant()
        {
            Flows = new AuthFlow();
        }

        public Tenant(string name)
            : this()
        {
            Name = name;
        }

        public string Name { get; set; }
        public string TenantId { get; set; }
        public string Environment { get; set; }
        public string Identifier { get; set; }
        public TenantAuthenticationType AuthType { get; set; }

        public string ResourceName { get; set; }
        public string Url { get; set; }
        public string ClientId { get; set; }
        public string ClientKey { get; set; }

        public string AuthScope { get; set; }

        public AuthFlow Flows { get; }

        

        public string DecodedIdentifier => Encoding.Unicode.GetString(Identifier.FromBase64());

        public static string EncodeIdentifier(string identifier)
        {
            return Encoding.Unicode.GetBytes(identifier).ToBase64();
        }

        public string DecodedClientKey => string.IsNullOrWhiteSpace(ClientKey) ? "" : Encoding.Unicode.GetString(ClientKey.FromBase64());

        public static string EncodeClientKey(string clientKey)
        {
            return Encoding.Unicode.GetBytes(clientKey).ToBase64();
        }


        public string HashKey()
        {
            return "0|1".FormatWith(Environment, TenantId);
        }
    }

    public class AuthFlow
    {
        public string Signup { get; set; }
        public string Password { get; set; }
        public string Account { get; set; }
    }
}