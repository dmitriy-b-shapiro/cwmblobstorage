﻿using System.Collections.Generic;

namespace Cwm.Common.Models
{
    /// <summary>
    /// Filter class for assessments
    /// </summary>
    public class AssessmentQuery : PagedAndSortedQuery
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssessmentQuery"/>
        /// </summary>
        public AssessmentQuery()
        {
            SortBy = "description";
            SortDirection = "asc";
        }

        /// <summary>
        /// Gets or Sets Search Term
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or Sets AssessmentTypeId
        /// </summary>
        public short? AssessmentTypeId { get; set; }

        /// <summary>
        /// Gets or Sets AssessmentCategoryId
        /// </summary>
        public short[] AssessmentCategoryIds { get; set; }
    }
}
