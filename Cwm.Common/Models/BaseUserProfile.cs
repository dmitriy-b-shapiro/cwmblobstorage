﻿using System;

namespace Cwm.Common.Models
{
    [Serializable]
    public abstract class BaseUserProfile
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string TelephoneNumber { get; set; }
    }
}