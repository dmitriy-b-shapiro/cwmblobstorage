﻿using System;
using System.Collections.Generic;

namespace Cwm.Common.Models
{
    [Serializable]
    public class PagedResult<T>
    {
        public List<T> Items { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
    }
}