﻿using System.Collections.Generic;
using Cwm.Common.Interfaces;

namespace Cwm.Common.Models
{
    public class FixedUserSession : IUserSession
    {
        public FixedUserSession()
        {
            // need this for serialization
        }

        public FixedUserSession(IUserSession source)
        {
            UserId = source.UserId;
            UserPrincipalName = source.UserPrincipalName;
            TenantId = source.TenantId;
            UserHostAddress = source.UserHostAddress;
            IsAdmin = source.IsAdmin;
            IsLocalAdmin = source.IsLocalAdmin;
            Roles = source.Roles;
            HasAccessToDataSourceIds = source.HasAccessToDataSourceIds;
            IsExternalUser = source.IsExternalUser;
            B2CFlowName = source.B2CFlowName;
        }

        public string UserId { get; set; }
        public string UserPrincipalName { get; set; }
        public string TenantId { get; set; }

        public string TenantIdAad { get; }
        public string UserHostAddress { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsLocalAdmin { get; set; }
        public bool IsExternalUser { get; }
        public List<string> Roles { get; set; }
        public string HashKey => TenantId + "|" + UserId;
        public bool PersistingToken { get; set; }
        public List<long> HasAccessToDataSourceIds { get; }
        public string B2CFlowName { get; set; }
    }
}