﻿using System;

namespace Cwm.Common.Models
{
    [Serializable]
    public class UserProfile : BaseUserProfile
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string UserPrincipalName { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsSignInEnabled { get; set; }
    }
}