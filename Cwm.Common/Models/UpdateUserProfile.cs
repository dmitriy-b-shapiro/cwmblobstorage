﻿using System.ComponentModel.DataAnnotations;

namespace Cwm.Common.Models
{
    public class UpdateUserProfile : BaseUserProfile
    {
        [Required]
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string UserPrincipalName { get; set; }
        public bool? IsSignInEnabled { get; set; }
    }
}