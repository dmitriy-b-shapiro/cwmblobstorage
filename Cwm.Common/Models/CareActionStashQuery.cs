﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cwm.Common.Models
{
    public class CareActionStashQuery : PagedAndSortedQuery
    {
        public CareActionStashQuery()
        {
            SortBy = "CreatedDate";
            SortDirection = "desc";
        }

        public long PatientProgramId { get; set; }
        public long? CareActionTypeId { get; set; }
        public long? PatientProgramEventId { get; set; }
    }
}
