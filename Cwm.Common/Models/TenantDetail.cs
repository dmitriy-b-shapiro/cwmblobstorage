﻿using System;
using System.Collections.Generic;

namespace Cwm.Common.Models
{
    [Serializable]
    public class TenantDetail
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string DefaultDomain { get; set; }
        public List<string> Domains { get; set; }
    }
}