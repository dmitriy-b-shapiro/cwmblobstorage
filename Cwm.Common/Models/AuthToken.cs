using System;
using Humanizer;

namespace Cwm.Common.Models
{
    [Serializable]
    public class AuthToken
    {
        public string Token { get; set; }
        public string SignedInUser { get; set; }
        public string ResourceId { get; set; }
        public string TenantId { get; set; }
        public string TokenRequestorUser { get; set; }
        public DateTime AcquiredAt { get; set; }
        public DateTimeOffset Expiration { get; set; }
        public string RefreshToken { get; set; }

        public bool IsExpired()
        {
            return Expiration <= DateTime.UtcNow;
        }

        public int ExpiresInSeconds()
        {
            return (int) (Expiration.UtcDateTime - DateTime.UtcNow).TotalSeconds;
        }

        public override string ToString()
        {
            return "TenantId:{0} \nSignedInUser:{1} \nResourceId:{2} \nAcquiredAt:{3} \nExpiration:{4} \nHasRefreshToken:{5}"
                .FormatWith(TenantId, SignedInUser, ResourceId, AcquiredAt, Expiration, !string.IsNullOrWhiteSpace(RefreshToken));
        }
    }
}