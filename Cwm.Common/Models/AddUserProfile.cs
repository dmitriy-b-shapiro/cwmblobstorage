﻿using System.ComponentModel.DataAnnotations;

namespace Cwm.Common.Models
{
    public class AddUserProfile : BaseUserProfile
    {
        [Required]
        public string DisplayName { get; set; }
        [Required]
        public string UserPrincipalName { get; set; }
        public bool ForceChangePasswordNextLogin { get; set; }
        public bool IsActive { get; set; }
    }
}
