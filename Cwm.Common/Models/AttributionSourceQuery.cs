﻿namespace Cwm.Common.Models
{
    public class AttributionSourceQuery : PagedAndSortedQuery
    {
        /// <summary>
        /// AttributionSourceQuery
        /// </summary>
        public long? DataSourceId { get; set; }
    }

    public class AttributionSourceQueryUnPaged
    {
        public long DataSourceId { get; set; }
        public bool ActiveOnly { get; set; }
        public string NameSearch { get; set; }
        public string CodeSearch { get; set; }
    }
}