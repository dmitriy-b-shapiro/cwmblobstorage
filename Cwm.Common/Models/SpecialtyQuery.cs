﻿namespace Cwm.Common.Models
{
    public class SpecialtyQuery : PagedAndSortedQuery
    {
        /// <summary>
        /// The Provider's Data Source
        /// </summary>
        public long? DataSourceId { get; set; }
    }
}