﻿using System.ComponentModel;

namespace Cwm.Common.Models
{

    // Note: If change this enumeration, change getPatientProgramStateEnum function in angular ui
    public enum PatientProgramStateEnum
    {
        [Description("Approval Pending")]
        ApprovalPending,
        Active,
        Suspended,
        [Description("Opted Out")]
        OptedOut,
        Stopped,
        Completed,
        [Description("Not Approved")]
        NotApproved
    }

}
