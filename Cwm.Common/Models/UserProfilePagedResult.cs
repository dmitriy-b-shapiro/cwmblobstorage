﻿using System.Collections.Generic;

namespace Cwm.Common.Models
{
    public class UserProfilePagedResult
    {
        public UserProfilePagedResult()
        {
            Items = new List<UserProfile>();
        }

        public List<UserProfile> Items { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public bool HasMorePages { get; set; }
    }
}