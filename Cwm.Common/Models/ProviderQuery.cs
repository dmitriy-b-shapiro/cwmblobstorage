﻿namespace Cwm.Common.Models
{
    /// <summary>
    /// Provider Query
    /// </summary>
    public class ProviderQuery : PagedAndSortedQuery
    {
        /// <summary>
        /// The Provider's Data Source
        /// </summary>
        public long? DataSourceId { get; set; }
    }
}