﻿namespace Cwm.Common.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PagedAndSortedQuery : PagedQuery
    {
        /// <summary>
        /// Sort by property (see remarks)
        /// </summary>
        public string SortBy { get; set; }
        /// <summary>
        /// Sort Direction (asc or desc)
        /// </summary>
        public string SortDirection { get; set; }
    }
}