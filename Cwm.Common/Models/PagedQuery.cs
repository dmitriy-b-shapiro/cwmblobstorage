﻿namespace Cwm.Common.Models
{
    public class PagedQuery
    {
        public PagedQuery()
        {
            Page = 1;
            ItemsPerPage = 10;
        }

        /// <summary>
        /// The page to return (default 1)
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Items per page (default 10)
        /// </summary>
        public int ItemsPerPage { get; set; }
        
    }
}