﻿namespace Cwm.Common.Models
{
    public class InsuranceQuery : PagedAndSortedQuery
    {
        /// <summary>
        /// The Provider's Data Source
        /// </summary>
        public long? DataSourceId { get; set; }
    }

    public class InsuranceQueryUnPaged
    {
        public long DataSourceId { get; set; }
        public bool ActiveOnly { get; set; }
        public string NameSearch { get; set; }
    }
}