﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using Cwm.Common.Extensions;

namespace Cwm.Common.Encryption
{
    public static class AesExtensions
    {
        public static string EncryptString(this Aes aes, string data)
        {
            return Encrypt(aes, Encoding.Unicode.GetBytes(data)).ToBase64();
        }

        public static byte[] Encrypt(this Aes aes, byte[] data)
        {
            using (var msEncrypted = new MemoryStream())
            using (var encryptor = aes.CreateEncryptor())
            {
                using (var csEncrypt = new CryptoStream(msEncrypted, encryptor, CryptoStreamMode.Write))
                {
                    using (var inStream = new MemoryStream(data))
                    {
                        inStream.CopyTo(csEncrypt);
                    }
                    csEncrypt.Close();
                }
                return msEncrypted.ToArray();
            }
        }

        public static string DecryptString(this Aes aes, string data)
        {
            return Encoding.Unicode.GetString(Decrypt(aes, data.FromBase64()));
        }

        public static byte[] Decrypt(this Aes aes, byte[] data)
        {
            using (var decryptor = aes.CreateDecryptor())
            using (var msDecrypted = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msDecrypted, decryptor, CryptoStreamMode.Write))
                {
                    csEncrypt.Write(data, 0, data.Length);
                }
                return msDecrypted.ToArray();
            }
        }

        public static string ToSecretString(this Aes aes)
        {
            return aes.Key.ToBase64() + "," + aes.IV.ToBase64();
        }
    }
}
