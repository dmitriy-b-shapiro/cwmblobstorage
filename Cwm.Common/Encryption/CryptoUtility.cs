﻿using System;
using System.Security.Cryptography;
using Cwm.Common.Extensions;

namespace Cwm.Common.Encryption
{
    public static class CryptoUtility
    {
        /// <summary>
        /// Helper method to generate random salt from 4 to 8 bits 
        /// </summary>
        /// <returns></returns>
        internal static byte[] CreateSaltBytes()
        {
            // Allocate a byte array, which will hold the salt.
            const long longZero = 0;
            var saltBytes = BitConverter.GetBytes(longZero);

            // Initialize a random number generator.
            var rng = new RNGCryptoServiceProvider();

            // Fill the salt with cryptographically strong byte values.
            rng.GetNonZeroBytes(saltBytes);

            return saltBytes;
        }

        /// <summary>
        /// Helper method returns SaltByte to Int64
        /// </summary>
        /// <returns></returns>
        internal static Int64 CreateSalt()
        {
            return BitConverter.ToInt64(CreateSaltBytes(), 0);
        }

        private static readonly RandomNumberGenerator Random = RandomNumberGenerator.Create();

        public static string CreateRandomToken()
        {
            var buffer = new byte[32];
            Random.GetNonZeroBytes(buffer);

            return Convert.ToBase64String(buffer)
                .Trim('=')
                .Replace('+', '-')
                .Replace('/', '_');
        }

        public static Aes CreateSymmetricKey(byte[] key, byte[] iv)
        {
            return new AesManaged { Key = key, IV = iv };
        }

        public static Aes CreateSymmetricKey(string keyVals)
        {
            return CreateSymmetricKey(keyVals.Split(',')[0].FromBase64(), keyVals.Split(',')[1].FromBase64());
        }

        public static Aes CreateSymmetricKey()
        {
            var aes = Aes.Create();
            aes.GenerateIV();
            aes.GenerateKey();
            return aes;
        }
    }
}
