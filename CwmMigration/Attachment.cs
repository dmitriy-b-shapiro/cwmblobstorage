﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwmMigration
{
    internal class Attachment
    {
		public Guid Id {get;set;}
		public string Title {get;set;}
		public string FileName {get;set;}
		public DateTime CreatedUtc {get;set;}
		public DateTime ModifiedUtc {get;set;}
		public string TimeStamp { get; set; }
		public bool Encrypted {get;set;}
		public string CreatedByUser_Id {get;set;}
		public Guid Patient_Id {get;set;}
    }
}
