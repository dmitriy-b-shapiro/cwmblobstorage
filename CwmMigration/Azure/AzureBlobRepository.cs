﻿using AzureEncryptionExtensions;
using AzureEncryptionExtensions.Providers;
using Cwm.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CwmMigration.Azure
{
    internal class AzureBlobRepository
    {

        private const string CacheKey = "blobContainer";
        private readonly CloudStorageAccount _storageAccount;

        private readonly IConfiguration _config;

        public AzureBlobRepository(IConfiguration config)
        {
            _config = config;
            _storageAccount = CloudStorageAccount.Parse(_config.GetSection("Azure:StorageConnectionString").Value);
        }

        private CloudBlockBlob GetBlockBlobReference(string tenantId, string name,
            IUserSession userSessionOverride = null)
        {

            var blobClient = _storageAccount.CreateCloudBlobClient();
            var blobContainer = blobClient.GetContainerReference(tenantId.ToLower());

            return blobContainer.GetBlockBlobReference(name);
        }

        public async Task GetDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var key = Convert.FromBase64String(_config.GetSection("Azure:CertificateKey").Value);
            var provider = new SymmetricBlobCryptoProvider(key);

            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, id.ToString().ToLower());
            if (await blockBlob.ExistsAsync())
            {
                await blockBlob.DownloadToStreamEncryptedAsync(provider, stream);
            }                
        }

        public async Task SaveDocumentAsync(IUserSession userSession, Guid id, Stream stream)
        {
            var key = Convert.FromBase64String(_config.GetSection("Azure:CertificateKey").Value);
            var provider = new SymmetricBlobCryptoProvider(key);

            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, id.ToString().ToLower());
            blockBlob.UploadFromStreamEncrypted(provider, stream);

            await Task.CompletedTask;
        }

        public async Task DeleteDocumentAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, id.ToString().ToLower(), userSession);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task<string> GetDocumentUnencryptedAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, id.ToString().ToLower(), userSession);
            return await blockBlob.DownloadTextAsync();
        }


        public async Task SaveDataUnencryptedAsync(IUserSession userSession, string uniqueId, string data)
        {
            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, uniqueId.ToLower(), userSession);
            await blockBlob.UploadTextAsync(data);
        }

        public async Task<(byte[] data, string filename)> GetFileAsync(IUserSession userSession, Guid id)
        {
            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, id.ToString().ToLower(), userSession);

            await blockBlob.FetchAttributesAsync();
            var filename = blockBlob.Metadata["filename"];
            var maxSize = Convert.ToInt32(blockBlob.Metadata["size"]);
            var data = new byte[maxSize];
            await blockBlob.DownloadToByteArrayAsync(data, 0);

            return (data, filename);

        }


        public async Task SaveFileAsync(IUserSession userSession, string uniqueId, string filename, byte[] data)
        {
            int size = data.Length;
            var blockBlob = GetBlockBlobReference(_config.GetSection("CWM:TenantId").Value, uniqueId.ToLower(), userSession);
            blockBlob.Metadata["filename"] = filename;
            blockBlob.Metadata["size"] = size.ToString();
            await blockBlob.UploadFromByteArrayAsync(data, 0, size);
        }

    }
}
