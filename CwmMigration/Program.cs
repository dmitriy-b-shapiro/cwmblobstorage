﻿// See https://aka.ms/new-console-template for more information
using CwmMigration;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using CwmMigration.AWS;
using CwmMigration.Azure;


IConfiguration configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

AzureBlobRepository azureBlobRepository = new AzureBlobRepository(configuration);
AWSBlobRepository awsBlobRepository = new AWSBlobRepository(configuration);

var connectionString = configuration.GetConnectionString("DefaultConnection");
using (var connection = new SqlConnection(connectionString))
{
    connection.Open();
    var sql = "SELECT * FROM Attachments";
    var attachments = connection.Query<Attachment>(sql).ToList();

    foreach(var attachment in attachments)
    {
        using (var memStream = new MemoryStream())
        {
            await azureBlobRepository.GetDocumentAsync(null, attachment.Id, memStream);
            if (memStream.Length > 0)
            {
                await awsBlobRepository.SaveDocumentAsync(null, attachment.Id, memStream);
            }
        }
    }
}
